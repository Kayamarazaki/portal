package com.feistyfelinesbreedables.portal.engine;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import javax.persistence.EntityManager;

import com.feistyfelinesbreedables.portal.entity.Body;
import com.feistyfelinesbreedables.portal.entity.Cat;
import com.feistyfelinesbreedables.portal.entity.CatType;
import com.feistyfelinesbreedables.portal.entity.Ear;
import com.feistyfelinesbreedables.portal.entity.EyeColor;
import com.feistyfelinesbreedables.portal.entity.EyeShape;
import com.feistyfelinesbreedables.portal.entity.Pelt;
import com.feistyfelinesbreedables.portal.entity.PeltColor;
import com.feistyfelinesbreedables.portal.entity.PupilShape;
import com.feistyfelinesbreedables.portal.entity.Tail;
import com.feistyfelinesbreedables.portal.entity.Trait;
import com.feistyfelinesbreedables.portal.entity.Whiskers;

public class BreedingEngine {
	private BreedingEngine()
	{
		
	}
	private Random random;
	private static BreedingEngine instance;
	
	public static BreedingEngine getInstance()
	{
		if(instance == null)
		{
			instance = new BreedingEngine();
			instance.random = new Random(System.currentTimeMillis());
		}
		return instance;
		
	}
	
	private Trait rollTrait(List<Trait> traits)
	{

		int sum = 0;
		for(Trait trait: traits)
		{
			sum += trait.getOccurence();
		}
		int roll = random.nextInt(sum);
		Collections.shuffle(traits);
		int counter = 0;
		for(Trait trait: traits)
		{
			counter += trait.getOccurence();
			if(counter > roll)
			{
				return trait;
				
			}
		}
		return null;			
		
	}
	
	private List<Trait> toTraitList(List input)
	{
		List<Trait> ret = new ArrayList<Trait>();
		
		for(Object o : input)
		{
			if(o instanceof Trait)
			{
				ret.add((Trait)o);
			}
		}
		return ret;
	}
	
	
	private void generateStarterTraits(Cat starter, EntityManager em)
	{
		starter.setPelt((Pelt)rollTrait(toTraitList(em.createNamedQuery("getPeltStarter").getResultList())));
		starter.setPeltcolor((PeltColor)rollTrait(toTraitList(em.createNamedQuery("getPeltColorStarter").setParameter("pelt", starter.getPelt()).getResultList())));
		starter.setBody((Body)rollTrait(toTraitList(em.createNamedQuery("getBodyStarter").getResultList())));
		starter.setEar((Ear)rollTrait(toTraitList(em.createNamedQuery("getEarStarter").getResultList())));
		starter.setEyecolor((EyeColor)rollTrait(toTraitList(em.createNamedQuery("getEyeColorStarter").getResultList())));
		starter.setEyeshape((EyeShape)rollTrait(toTraitList(em.createNamedQuery("getEyeShapeStarter").getResultList())));
		starter.setPupilshape((PupilShape)rollTrait(toTraitList(em.createNamedQuery("getPupilShapeStarter").getResultList())));
		starter.setTail((Tail)rollTrait(toTraitList(em.createNamedQuery("getTailStarter").getResultList())));
		starter.setWhiskers((Whiskers)rollTrait(toTraitList(em.createNamedQuery("getWhiskersStarter").getResultList())));
	}
	
	public void generateStarter(Cat starter,CatGenerationType type, EntityManager em)
	{
		switch(type)
		{
			case WildStarterFemale:
			{
				starter.setCattype((CatType)em.createNamedQuery("getCatTypeByName").setParameter("name", "Wild Cat").getResultList().get(0));
				generateStarterTraits(starter,em);
				starter.setMale(false);
				break;
			}
			case WildStarterMale:
			{
				starter.setCattype((CatType)em.createNamedQuery("getCatTypeByName").setParameter("name", "Wild Cat").getResultList().get(0));
				generateStarterTraits(starter,em);
				starter.setMale(true);
				break;
			}
			case BigStarterFemale:
			{
				starter.setCattype((CatType)em.createNamedQuery("getCatTypeByName").setParameter("name", "Big Cat").getResultList().get(0));
				generateStarterTraits(starter,em);
				starter.setMale(false);
				break;
			}
			case BigStarterMale:
			{
				starter.setCattype((CatType)em.createNamedQuery("getCatTypeByName").setParameter("name", "Big Cat").getResultList().get(0));
				generateStarterTraits(starter,em);
				starter.setMale(true);
				break;
			}
			case FeistyStarterFemale:
			{
				starter.setCattype((CatType)em.createNamedQuery("getCatTypeByName").setParameter("name", "Feisty").getResultList().get(0));
				generateStarterTraits(starter,em);
				starter.setMale(false);
				break;
			}
			case FeistyStarterMale:
			{
				starter.setCattype((CatType)em.createNamedQuery("getCatTypeByName").setParameter("name", "Feisty").getResultList().get(0));
				generateStarterTraits(starter,em);
				starter.setMale(true);
				break;
			}
		}
		
	}
	
	
	
	
	public void generateOffspring(Cat offspring, EntityManager em) {
		ArrayList<Cat> ancestors = new ArrayList<Cat>();
		
		int r = random.nextInt(2);
		
		if(r == 0)
		{
			offspring.setMale(false);
		}
		else
		{
			offspring.setMale(true);
		}
		
		buildAncestorTree(ancestors, offspring.getFather(), offspring.getMother());
		generateCatType(ancestors, offspring);
		generateEar(ancestors, offspring, em);
		generateBody(ancestors, offspring, em);
		generateEyes(ancestors, offspring, em);
		generatePelt(ancestors, offspring, em);
		generateTail(ancestors,offspring, em);
		generateWhiskers(ancestors,offspring, em);
		
	}
	
	private void generateCatType(ArrayList<Cat> ancestors, Cat offspring)
	{
		int sum = 0;
		for(Cat cat: ancestors)
		{
			sum += cat.getCattype().getOccurence();
		}
		int roll = random.nextInt(sum);
		Collections.shuffle(ancestors);
		int counter = 0;
		for(Cat cat : ancestors)
		{
			counter += cat.getCattype().getOccurence();
			if(counter > roll)
			{
				offspring.setCattype(cat.getCattype());
				break;
			}
		}
		
	}
	
	private void generateBody(ArrayList<Cat> ancestors, Cat offspring, EntityManager em)
	{
		int sum = 0;
		
		int roll = random.nextInt(100);
		List<Body> list = em.createNamedQuery("getBodyBreeding", Body.class).getResultList();
		
		if(list.size() > 0 && roll < 15)
		{
			for(Body body: list)
			{
				sum += body.getOccurence();
			}
			roll = random.nextInt(sum);
			Collections.shuffle(list);
			int counter = 0;
			for(Body body : list)
			{
				counter += body.getOccurence();
				if(counter > roll)
				{
					offspring.setBody(body);
					break;
				}
			}
		}
		else
		{
			for(Cat cat: ancestors)
			{
				sum += cat.getBody().getOccurence();
			}
			roll = random.nextInt(sum);
			Collections.shuffle(ancestors);
			int counter = 0;
			for(Cat cat : ancestors)
			{
				counter += cat.getBody().getOccurence();
				if(counter > roll)
				{
					offspring.setBody(cat.getBody());
					break;
				}
			}
		}
		
	}
	
	private void generateEar(ArrayList<Cat> ancestors, Cat offspring, EntityManager em)
	{
		int sum = 0;

		int roll = random.nextInt(100);
		List<Ear> list = em.createNamedQuery("getEarBreeding", Ear.class).getResultList();
		
		if(list.size() > 0 && roll < 15)
		{
			for(Ear ear: list)
			{
				sum += ear.getOccurence();
			}
			roll = random.nextInt(sum);
			Collections.shuffle(list);
			int counter = 0;
			for(Ear ear : list)
			{
				counter += ear.getOccurence();
				if(counter > roll)
				{
					offspring.setEar(ear);
					break;
				}
			}
		}
		else
		{
			for(Cat cat: ancestors)
			{
				sum += cat.getEar().getOccurence();
			}
			roll = random.nextInt(sum);
			Collections.shuffle(ancestors);
			int counter = 0;
			for(Cat cat : ancestors)
			{
				counter += cat.getEar().getOccurence();
				if(counter > roll)
				{
					offspring.setEar(cat.getEar());
					break;
				}
			}
		}
		
	}
	
	private void generateEyeColor(ArrayList<Cat> ancestors, Cat offspring, EntityManager em)
	{
		int sum = 0;
		int roll = random.nextInt(100);
		List<EyeColor> list = em.createNamedQuery("getEyeColorBreeding", EyeColor.class).getResultList();
		
		if(list.size() > 0 && roll < 15)
		{
			for(EyeColor ec: list)
			{
				sum += ec.getOccurence();
			}
			roll = random.nextInt(sum);
			Collections.shuffle(list);
			int counter = 0;
			for(EyeColor ec: list)
			{
				counter += ec.getOccurence();
				if(counter > roll)
				{
					offspring.setEyecolor(ec);
					break;
				}
			}
		}
		else
		{
			for(Cat cat: ancestors)
			{
				sum += cat.getEar().getOccurence();
			} 
			roll = random.nextInt(sum);
			Collections.shuffle(ancestors);
			int counter = 0;
			for(Cat cat : ancestors)
			{
				counter += cat.getEyecolor().getOccurence();
				if(counter > roll)
				{
					offspring.setEyecolor(cat.getEyecolor());
					break;
				}
			}
		}
		
	}
	
	private void generateEyeShape(ArrayList<Cat> ancestors, Cat offspring, EntityManager em)
	{
		int sum = 0;
		int roll = random.nextInt(100);
		List<EyeShape> list = em.createNamedQuery("getEyeShapeBreeding", EyeShape.class).getResultList();
		
		if(list.size() > 0 && roll < 15)
		{
			for(EyeShape es: list)
			{
				sum += es.getOccurence();
			}
			roll = random.nextInt(sum);
			Collections.shuffle(list);
			int counter = 0;
			for(EyeShape es: list)
			{
				counter += es.getOccurence();
				if(counter > roll)
				{
					offspring.setEyeshape(es);
					break;
				}
			}
		}
		else
		{
			for(Cat cat: ancestors)
			{
				sum += cat.getEyeshape().getOccurence();
			}
			 roll = random.nextInt(sum);
			Collections.shuffle(ancestors);
			int counter = 0;
			for(Cat cat : ancestors)
			{
				counter += cat.getEyeshape().getOccurence();
				if(counter > roll)
				{
					offspring.setEyeshape(cat.getEyeshape());
					break;
				}
			}
		}
		
	}
	
	private void generatePupilShape(ArrayList<Cat> ancestors, Cat offspring, EntityManager em)
	{
		int sum = 0;
		int roll = random.nextInt(100);
		List<PupilShape> list = em.createNamedQuery("getPupilShapeBreeding", PupilShape.class).getResultList();
		
		if(list.size() > 0 && roll < 15)
		{
			for(PupilShape ps: list)
			{
				sum += ps.getOccurence();
			}
			roll = random.nextInt(sum);
			Collections.shuffle(list);
			int counter = 0;
			for(PupilShape ps: list)
			{
				counter += ps.getOccurence();
				if(counter > roll)
				{
					offspring.setPupilshape(ps);
					break;
				}
			}
		}
		else
		{
			for(Cat cat: ancestors)
			{
				sum += cat.getPupilshape().getOccurence();
			}
			 roll = random.nextInt(sum);
			Collections.shuffle(ancestors);
			int counter = 0;
			for(Cat cat : ancestors)
			{
				counter += cat.getPupilshape().getOccurence();
				if(counter > roll)
				{
					offspring.setPupilshape(cat.getPupilshape());
					break;
				}
			}
		}
		
	}
	
	private void generateWhiskers(ArrayList<Cat> ancestors, Cat offspring, EntityManager em)
	{
		int sum = 0;
		int roll = random.nextInt(100);
		List<Whiskers> list = em.createNamedQuery("getWhiskersBreeding", Whiskers.class).getResultList();
		
		if(list.size() > 0 && roll < 15)
		{
			for(Whiskers w: list)
			{
				sum += w.getOccurence();
			}
			roll = random.nextInt(sum);
			Collections.shuffle(list);
			int counter = 0;
			for(Whiskers w: list)
			{
				counter += w.getOccurence();
				if(counter > roll)
				{
					offspring.setWhiskers(w);
					break;
				}
			}
		}
		else
		{
			for(Cat cat: ancestors)
			{
				sum += cat.getWhiskers().getOccurence();
			}
			roll = random.nextInt(sum);
			Collections.shuffle(ancestors);
			int counter = 0;
			for(Cat cat : ancestors)
			{
				counter += cat.getWhiskers().getOccurence();
				if(counter > roll)
				{
					offspring.setWhiskers(cat.getWhiskers());
					break;
				}
			}
		}
		
	}
	
	private void generateTail(ArrayList<Cat> ancestors, Cat offspring, EntityManager em)
	{
		int sum = 0;
		int roll = random.nextInt(100);
		List<Tail> list = em.createNamedQuery("getTailBreeding", Tail.class).getResultList();
		
		if(list.size() > 0 && roll < 15)
		{
			for(Tail t: list)
			{
				sum += t.getOccurence();
			}
			roll = random.nextInt(sum);
			Collections.shuffle(list);
			int counter = 0;
			for(Tail t: list)
			{
				counter += t.getOccurence();
				if(counter > roll)
				{
					offspring.setTail(t);
					break;
				}
			}
		}
		else
		{
			for(Cat cat: ancestors)
			{
				sum += cat.getTail().getOccurence();
			}
			roll = random.nextInt(sum);
			Collections.shuffle(ancestors);
			int counter = 0;
			for(Cat cat : ancestors)
			{
				counter += cat.getTail().getOccurence();
				if(counter > roll)
				{
					offspring.setTail(cat.getTail());
					break;
				}
			}
		}
		
	}
	
	private void generatePelt(ArrayList<Cat> ancestors, Cat offspring, EntityManager em)
	{
		int sum = 0;
		int roll = random.nextInt(100);
		List<PeltColor> list = em.createNamedQuery("getPeltColorBreeding", PeltColor.class).getResultList();
		
		if(list.size() > 0 && roll < 15)
		{
			for(PeltColor pc: list)
			{
				sum += pc.getOccurence();
			}
			roll = random.nextInt(sum);
			Collections.shuffle(list);
			int counter = 0;
			for(PeltColor pc: list)
			{
				counter += pc.getOccurence();
				if(counter > roll)
				{
					offspring.setPeltcolor(pc);
					offspring.setPelt(pc.getPelt());
					break;
				}
			}
		}
		else
		{
			for(Cat cat: ancestors)
			{
				sum += cat.getPelt().getOccurence();
			}
			roll = random.nextInt(sum);
			Collections.shuffle(ancestors);
			int counter = 0;
			for(Cat cat : ancestors)
			{
				counter += cat.getPelt().getOccurence();
				if(counter > roll)
				{
					offspring.setPelt(cat.getPelt());
					break;
				}
			}
			generatePeltColor(ancestors,offspring, em);
		}
	
	}
	
	private void generatePeltColor(ArrayList<Cat> ancestors, Cat offspring, EntityManager em)
	{
		int sum = 0;
		List<PeltColor> peltcolors = offspring.getPelt().getPeltcolors();
				
		for(Cat cat: ancestors)
		{
			if(peltcolors.contains(cat.getPeltcolor()))
				sum += cat.getPeltcolor().getOccurence();
		}
		int roll = random.nextInt(sum);
		Collections.shuffle(ancestors);
		int counter = 0;
		for(Cat cat : ancestors)
		{
			if(peltcolors.contains(cat.getPeltcolor()))
			{
				counter += cat.getPeltcolor().getOccurence();
				if(counter > roll)
				{
					offspring.setPeltcolor(cat.getPeltcolor());
					break;
				}
			}
		}
				
	}
	
	private void generateEyes(ArrayList<Cat> ancestors, Cat offspring, EntityManager em)
	{
		generateEyeColor(ancestors,offspring, em);
		generateEyeShape(ancestors,offspring, em);
		generatePupilShape(ancestors,offspring, em);
		
		
		
	}
	
	private void buildAncestorTree(List<Cat> ancestors, Cat father, Cat mother)
	{
		
		ancestors.add(father);
		ancestors.add(mother);
		if(father.getFather() != null && father.getMother() != null)
			buildAncestorTree(ancestors, father.getFather(), father.getMother());
		if(mother.getFather() != null && mother.getMother() != null)
		buildAncestorTree(ancestors, mother.getFather(), mother.getMother());
	}
}
