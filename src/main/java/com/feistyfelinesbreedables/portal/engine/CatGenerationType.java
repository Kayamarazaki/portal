package com.feistyfelinesbreedables.portal.engine;

public enum CatGenerationType
{
	WildStarterMale(0), 
	WildStarterFemale(1),
	BigStarterMale(2),
	BigStarterFemale(3),
	FeistyStarterMale(4),
	FeistyStarterFemale(5);
	

    private final int value;
    private CatGenerationType(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
	
}

