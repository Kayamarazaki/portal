package com.feistyfelinesbreedables.portal.repository;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.feistyfelinesbreedables.portal.entity.Cat;

public interface CatRepository extends CrudRepository<Cat, Long> {
    void delete(Cat deleted);
    
    List<Cat> findAll();
 
    Cat findOne(Long id);
 
    Cat save(Cat persisted);
    
    
}
