package com.feistyfelinesbreedables.portal.repository;
import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.feistyfelinesbreedables.portal.entity.Vendor;

public interface VendorRepository extends CrudRepository<Vendor, Long> {
    void delete(Vendor deleted);
    
    List<Vendor> findAll();
 
    Vendor findOne(Long id);
 
    Vendor save(Vendor persisted);
}
