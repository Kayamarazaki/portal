package com.feistyfelinesbreedables.portal.config;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.LocalEntityManagerFactoryBean;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import com.feistyfelinesbreedables.portal.scheduler.Scheduler;
@EnableWebMvc
@Configuration
@EnableJpaRepositories(basePackages = "com.feistyfelinesbreedables.portal.repository")
@ComponentScan(basePackages = { "com.feistyfelinesbreedables.portal" })
@EnableTransactionManagement
@EnableScheduling
public class MvcConfig extends WebMvcConfigurerAdapter {
	@Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/images/**").addResourceLocations("/WEB-INF/images/");
        registry.addResourceHandler("/js/**").addResourceLocations("/WEB-INF/js/");
        registry.addResourceHandler("/css/**").addResourceLocations("/WEB-INF/css/");
    }
	@Override
    public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
        configurer.enable();
    }
    @Bean
    public Scheduler myScheduler()
    {
    	return new Scheduler();
    }
    
    @Bean
	public PasswordEncoder passwordEncoder(){
		PasswordEncoder encoder = new BCryptPasswordEncoder();
		return encoder;
	}
    
    @Bean
    public InternalResourceViewResolver jspViewResolver()
    {
    	InternalResourceViewResolver bean = new InternalResourceViewResolver();
        bean.setPrefix("/WEB-INF/views/");
        bean.setSuffix(".jsp");
        return bean;
    }
    
    @Bean
    public DataSource basicDataSource()
    {
    	DriverManagerDataSource source = new DriverManagerDataSource();
    	source.setDriverClassName("com.mysql.jdbc.Driver");
        source.setUrl("jdbc:mysql://localhost:3306/ffb_backend");
        source.setUsername("root");
        //Local PW:
        //source.setPassword("root");
        //Remote PW:
        source.setPassword("ptZeWnNoZXZdmvjksceQ");
    	
    	return source;
    }
    
    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory()
    {
    	LocalContainerEntityManagerFactoryBean bean = new LocalContainerEntityManagerFactoryBean();
    	bean.setPackagesToScan("com.feistyfelinesbreedables.portal.entity");
    	bean.setPersistenceUnitName("persistenceUnit");
    	bean.setDataSource(basicDataSource());
    	return bean;
    }
    
    @Bean
    public JpaTransactionManager transactionManager()
    {
    	JpaTransactionManager bean = new JpaTransactionManager();
    	bean.setPersistenceUnitName("persistenceUnit");
    	
    	return bean;
    }
}