package com.feistyfelinesbreedables.portal.scheduler;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.feistyfelinesbreedables.portal.entity.Cat;

@Component("scheduler")
@Transactional
public class Scheduler {
	public static  double GROWTH_RATE = 100d/48d;
	
	@Autowired
	EntityManager em;
	
	@Scheduled(fixedRate = 300000)
	public void serverTick()
	{
		processHunger();
		processHappiness();
		buildHeat();
		buildPregnancy();
	}
	

	private void buildPregnancy() {
		Timestamp now = new Timestamp(System.currentTimeMillis());
		Timestamp lu = getLU();
		Timestamp lhc = getLC();
		List<Cat> li = em.createNamedQuery("getCatPregnant", Cat.class).setParameter("lastupdate", lu).setParameter("lastpregnancychange", lhc).getResultList();
		
				
		for(Cat cat: li)
		{
			cat.processPregnancy(em, lu, lhc, now);
		}		
	}


	private void processHappiness() {
		Timestamp now = getNow();
		Timestamp lu = getLU();
		Timestamp lhc = getLC();
		List<Cat> li = em.createNamedQuery("getCatHUHA", Cat.class).setParameter("lastupdate",lu).getResultList();
		List<Cat> li2 = em.createNamedQuery("getCatInactive",Cat.class).setParameter("lastupdate",lu).getResultList();
		li.addAll(li2);
				
		for(Cat cat: li)
		{
			cat.processHappiness(em, lu, lhc, now);
		}
	}


	private void processHunger() {
		Timestamp now = getNow();
		Timestamp lu = getLU();
		Timestamp lhc = getLC();
		List<Cat> li = em.createNamedQuery("getCatHUHA", Cat.class).setParameter("lastupdate",lu).getResultList();
		List<Cat> li2 = em.createNamedQuery("getCatInactive",Cat.class).setParameter("lastupdate",lu).getResultList();
		li.addAll(li2);
		

		for(Cat cat: li)
		{
			cat.processHunger(em, lu, lhc, now);
		}
	}


	private void buildHeat()
	{
		Timestamp now = getNow();
		Timestamp lu = getLU();
		Timestamp lhc = getLC();
		List<Cat> li = em.createNamedQuery("getCatHUHA", Cat.class).setParameter("lastupdate",lu).getResultList();
		for(Cat cat: li)
		{
			cat.processHeat(em, lu, lhc, now);
		}		
	}
	
	private Timestamp getLC()
	{
		return new Timestamp(System.currentTimeMillis() - ((3600 * 1000) - 250));
	}
	
	private Timestamp getNow()
	{
		return new Timestamp(System.currentTimeMillis());
	}
	
	private Timestamp getLU()
	{
		return new Timestamp(System.currentTimeMillis() - (15 * 60 * 1000));
	}
	
}
