package com.feistyfelinesbreedables.portal.service;

import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.feistyfelinesbreedables.portal.entity.Body;
import com.feistyfelinesbreedables.portal.entity.CatType;
import com.feistyfelinesbreedables.portal.entity.Ear;
import com.feistyfelinesbreedables.portal.entity.EyeColor;
import com.feistyfelinesbreedables.portal.entity.EyeShape;
import com.feistyfelinesbreedables.portal.entity.Eyes;
import com.feistyfelinesbreedables.portal.entity.Pelt;
import com.feistyfelinesbreedables.portal.entity.PeltColor;
import com.feistyfelinesbreedables.portal.entity.PupilShape;
import com.feistyfelinesbreedables.portal.entity.Tail;
import com.feistyfelinesbreedables.portal.entity.TraitType;
import com.feistyfelinesbreedables.portal.entity.Vendor;
import com.feistyfelinesbreedables.portal.entity.VendorLine;
import com.feistyfelinesbreedables.portal.entity.VendorObject;
import com.feistyfelinesbreedables.portal.entity.Whiskers;

@Service
@Transactional
public class CreationService {
	@Autowired
	EntityManager em;
	
	public void createStartingData()
	{
		int wildCatnum = em.createNamedQuery("getCatTypeByName").setParameter("name", "Wild Cat").getResultList().size();
		if(wildCatnum == 0)
		{
			createCatTypes();
		
			createTraits();
		
			createVendorObjects();
		
			createVendors();
		}
	}
	
	private void createVendors() 
	{		
		Vendor wildMale = CreateVendor("Wild Cat Single Male", 300,new String[] {"Feisty Felines Wild Cat Starter Male"}, new int[] {1});
		Vendor wildFemale = CreateVendor("Wild Cat Single Female", 300,new String[] {"Feisty Felines Wild Cat Starter Female"}, new int[] {1});
		
		Vendor bigMale = CreateVendor("Big Cat Single Male", 300,new String[] {"Feisty Felines Big Cat Starter Male"}, new int[] {1});
		Vendor bigFemale = CreateVendor("Big Cat Single Female", 300,new String[] {"Feisty Felines Big Cat Starter Female"}, new int[] {1});
		
		Vendor feistyMale = CreateVendor("Feisty Single Male", 300,new String[] {"Feisty Felines Feisty Starter Male"}, new int[] {1});
		Vendor feistyFemale = CreateVendor("Feisty Single Female", 300,new String[] {"Feisty Felines Feisty Starter Female"}, new int[] {1});
		
		Vendor wildPair = CreateVendor("Wild Cat Breeding Pair", 550,new String[] {"Feisty Felines Wild Cat Starter Male","Feisty Felines Wild Cat Starter Female"}, new int[] {1,1});
		Vendor bigPair = CreateVendor("Big Cat Breeding Pair", 550,new String[] {"Feisty Felines Big Cat Starter Male","Feisty Felines Big Cat Starter Female"}, new int[] {1,1});
		Vendor feistyPair = CreateVendor("Feisty Breeding Pair", 550,new String[] {"Feisty Felines Feisty Starter Male","Feisty Felines Feisty Starter Female"}, new int[] {1,1});
		
		Vendor wild10 = CreateVendor("Wild Cat 10", 2500,new String[] {"Feisty Felines Wild Cat Starter Male","Feisty Felines Wild Cat Starter Female"}, new int[] {4,6});
		Vendor big10 = CreateVendor("Big Cat 10", 2500,new String[] {"Feisty Felines Big Cat Starter Male","Feisty Felines Big Cat Starter Female"}, new int[] {4,6});
		Vendor feisty10 = CreateVendor("Feisty 10", 2500,new String[] {"Feisty Felines Feisty Starter Male","Feisty Felines Feisty Starter Female"}, new int[] {4,6});
		
		Vendor wild50 = CreateVendor("Wild Cat 50", 12000,new String[] {"Feisty Felines Wild Cat Starter Male","Feisty Felines Wild Cat Starter Female"}, new int[] {20,30});
		Vendor big50 = CreateVendor("Big Cat 50", 12000,new String[] {"Feisty Felines Big Cat Starter Male","Feisty Felines Big Cat Starter Female"}, new int[] {20,30});
		Vendor feisty50 = CreateVendor("Feisty 50", 12000,new String[] {"Feisty Felines Feisty Starter Male","Feisty Felines Feisty Starter Female"}, new int[] {20,30});
		
		Vendor wild100 = CreateVendor("Wild Cat 100", 23000,new String[] {"Feisty Felines Wild Cat Starter Male","Feisty Felines Wild Cat Starter Female"}, new int[] {40,60});
		Vendor big100 = CreateVendor("Big Cat 100", 23000,new String[] {"Feisty Felines Big Cat Starter Male","Feisty Felines Big Cat Starter Female"}, new int[] {40,60});
		Vendor feisty100 = CreateVendor("Feisty 100", 23000,new String[] {"Feisty Felines Feisty Starter Male","Feisty Felines Feisty Starter Female"}, new int[] {40,60});
		
		Vendor food4 = CreateVendor("4 Weeks of food", 150,new String[] {"Food Pack 4"}, new int[] {1});
		Vendor food40 = CreateVendor("40 Weeks of food", 1500,new String[] {"Food Pack 40"}, new int[] {1});
		Vendor food100 = CreateVendor("100 Weeks of food", 3750,new String[] {"Food Pack 100"}, new int[] {1});
		
		Vendor happiness4 = CreateVendor("4 Weeks of happiness", 75,new String[] {"Happiness Pack 4"}, new int[] {1});
		Vendor happiness40 = CreateVendor("40 Weeks of happiness", 750,new String[] {"Happiness Pack 40"}, new int[] {1});
		Vendor happiness100 = CreateVendor("100 Weeks of happiness", 1875,new String[] {"Happiness Pack 100"}, new int[] {1});
		
		Vendor foodbowl = CreateVendor("Food Bowl", 1, new String[]{"Food Bowl"},new int[]{1});
		Vendor happinessbowl = CreateVendor("Happiness Bowl", 1, new String[]{"Happiness Bowl"},new int[]{1});
		Vendor healthpack= CreateVendor("Health Pack", 250, new String[]{"Health Pack"},new int[]{1});
	}

	private Vendor CreateVendor(String name, int price, String[] names, int[] amounts)
	{
		if(names.length == amounts.length)
		{
			Vendor vendor = new Vendor(name,price);
			em.persist(vendor);
			for(int i = 0; i < names.length;i++)
			{
				em.persist(new VendorLine(vendor, (VendorObject)em.createNamedQuery("getVendorObjectByName").setParameter("name", names[i]).getResultList().get(0),amounts[i]));
			}
			return vendor;
		}
		return null;
	}
	
	private void createVendorObjects() 
	{
		VendorObject starterWildMale = new VendorObject("Feisty Felines Wild Cat Starter Male", "Feisty Felines Wild Cat Starter Male Basket");
		VendorObject starterWildFemale = new VendorObject("Feisty Felines Wild Cat Starter Female", "Feisty Felines Wild Cat Starter Female Basket");
		VendorObject starterBigMale = new VendorObject("Feisty Felines Big Cat Starter Male", "Feisty Felines Big Cat Starter Male Basket");
		VendorObject starterBigFemale = new VendorObject("Feisty Felines Big Cat Starter Female", "Feisty Felines Big Cat Starter Female Basket");
		VendorObject starterFeistyMale = new VendorObject("Feisty Felines Feisty Starter Male", "Feisty Felines Feisty Starter Male Basket");
		VendorObject starterFeistyFemale = new VendorObject("Feisty Felines Feisty Starter Female", "Feisty Felines Feisty Starter Female Basket");
		
		VendorObject happinessbowl = new VendorObject("Happiness Bowl", "FFB Happiness");
		VendorObject foodbowl = new VendorObject("Food Bowl", "FFB Food");
		VendorObject healthpack = new VendorObject("Health Pack", "FFB Health pack");
		
		VendorObject foodpack4 = new VendorObject("Food Pack 4", "FFB Food Pack 4 Weeks");
		VendorObject foodpack40 = new VendorObject("Food Pack 40", "FFB Food Pack 40 Weeks");
		VendorObject foodpack100 = new VendorObject("Food Pack 100", "FFB Food Pack 100 Weeks");
		
		VendorObject happinesspack4 = new VendorObject("Happiness Pack 4", "FFB Happiness Pack 4 Weeks");
		VendorObject happinesspack40 = new VendorObject("Happiness Pack 40", "FFB Happiness Pack 40 Weeks");
		VendorObject happinespack100 = new VendorObject("Happiness Pack 100", "FFB Happiness Pack 100 Weeks");
		
		em.persist(starterFeistyFemale);
		em.persist(starterFeistyMale);
		em.persist(starterBigFemale);
		em.persist(starterBigMale);
		em.persist(starterWildFemale);
		em.persist(starterWildMale);
		em.persist(happinessbowl);
		em.persist(foodbowl);
		em.persist(healthpack);
		em.persist(foodpack4);
		em.persist(foodpack40);
		em.persist(foodpack100);
		em.persist(happinesspack4);
		em.persist(happinesspack40);
		em.persist(happinespack100);
	}

	private void createTraits() 
	{
		Pelt solid = new Pelt("Solid");
		solid.setOccurence(100);
		solid.setTraittype(TraitType.Starter);
		em.persist(solid);

		PeltColor grey = new PeltColor("Grey",solid,"[6b7b6cbb-8c26-a464-ab8f-831566f4ed7f/1a69a82e-f6fc-0948-e293-843fa4972a5e/7b6a8049-20f4-e72c-8741-9e005b3adb78/4fa0a1a7-8e50-5246-73df-3bcf8918375e/4fa0a1a7-8e50-5246-73df-3bcf8918375e/d483bea2-a347-7cc9-60d9-40485c23d605/af939185-70ad-c10e-76ce-88c6e7e7a456/af939185-70ad-c10e-76ce-88c6e7e7a456/af939185-70ad-c10e-76ce-88c6e7e7a456/af939185-70ad-c10e-76ce-88c6e7e7a456/af939185-70ad-c10e-76ce-88c6e7e7a456/af939185-70ad-c10e-76ce-88c6e7e7a456/3f99cac4-4dc2-62b8-aaed-526832a9baea/3f99cac4-4dc2-62b8-aaed-526832a9baea/3f99cac4-4dc2-62b8-aaed-526832a9baea/3f99cac4-4dc2-62b8-aaed-526832a9baea/3f99cac4-4dc2-62b8-aaed-526832a9baea/3f99cac4-4dc2-62b8-aaed-526832a9baea]","[Body/Neck/Head/Ears Upright/Ears Drooped/Tail/LF Back/LF Front/LF Stand/RF Back/RF Front/RF Stand/LR Back/LR Front/LR Stand/RR Back/RR Front/RR Stand]");
		grey.setOccurence(35);
		grey.setTraittype(TraitType.Breeding);
		
		PeltColor white = new PeltColor("White",solid,"[60db5a9a-0f8a-a846-1b0d-10c75a3869b8/50214ed1-dc17-4e18-4ad8-33699c272d57/bee8c842-42e1-092c-7643-f19aac00b714/6f39585a-e147-7ab3-3cb2-7ff8e06449b9/6f39585a-e147-7ab3-3cb2-7ff8e06449b9/6fe8af57-e8c5-2e12-91d0-9520e02e1b41/c82b9abb-5837-4e2a-dd16-0af2b3adbb50/c82b9abb-5837-4e2a-dd16-0af2b3adbb50/c82b9abb-5837-4e2a-dd16-0af2b3adbb50/c82b9abb-5837-4e2a-dd16-0af2b3adbb50/c82b9abb-5837-4e2a-dd16-0af2b3adbb50/c82b9abb-5837-4e2a-dd16-0af2b3adbb50/f35442a7-746c-1366-3676-1645e7a5a9bf/f35442a7-746c-1366-3676-1645e7a5a9bf/f35442a7-746c-1366-3676-1645e7a5a9bf/f35442a7-746c-1366-3676-1645e7a5a9bf/f35442a7-746c-1366-3676-1645e7a5a9bf/f35442a7-746c-1366-3676-1645e7a5a9bf]","[Body/Neck/Head/Ears Upright/Ears Drooped/Tail/LF Back/LF Front/LF Stand/RF Back/RF Front/RF Stand/LR Back/LR Front/LR Stand/RR Back/RR Front/RR Stand]");
		white.setOccurence(100);
		white.setTraittype(TraitType.Starter);
		
		PeltColor black = new PeltColor("Black",solid,"[456b1853-f5fc-1d4e-db10-64b8730f37e3/8c9368b7-3ff4-ae99-0423-44edf81c02c4/750b3dbb-2f70-9704-df68-02fd51118146/8321a071-b620-f1f0-5133-95ac9e5e653e/8321a071-b620-f1f0-5133-95ac9e5e653e/02b221bd-ed23-2988-a72e-556971f77900/77b9a4ec-18ff-823b-10de-8c76eacf5e44/77b9a4ec-18ff-823b-10de-8c76eacf5e44/77b9a4ec-18ff-823b-10de-8c76eacf5e44/77b9a4ec-18ff-823b-10de-8c76eacf5e44/77b9a4ec-18ff-823b-10de-8c76eacf5e44/77b9a4ec-18ff-823b-10de-8c76eacf5e44/091f096c-5fc9-ef5b-0731-8b4fbd6d63a7/091f096c-5fc9-ef5b-0731-8b4fbd6d63a7/091f096c-5fc9-ef5b-0731-8b4fbd6d63a7/091f096c-5fc9-ef5b-0731-8b4fbd6d63a7/091f096c-5fc9-ef5b-0731-8b4fbd6d63a7/091f096c-5fc9-ef5b-0731-8b4fbd6d63a7]","[Body/Neck/Head/Ears Upright/Ears Drooped/Tail/LF Back/LF Front/LF Stand/RF Back/RF Front/RF Stand/LR Back/LR Front/LR Stand/RR Back/RR Front/RR Stand]");
		black.setOccurence(100);
		black.setTraittype(TraitType.Starter);
		
		PeltColor red= new PeltColor("Red", solid, "[7edef1a8-0f81-611d-5e0b-4941253568bf/e4db1134-daa5-a1a1-938a-b044c2134bd8/8751e73b-119a-cd7b-0a49-dc1cbad20fb3/ebc35d4a-1583-225f-be66-859a4041f8f5/ebc35d4a-1583-225f-be66-859a4041f8f5/3988edfe-965f-0fa1-1f30-8c25bc29fae9/ea61a27b-a46e-9161-565c-beea5df33a44/ea61a27b-a46e-9161-565c-beea5df33a44/ea61a27b-a46e-9161-565c-beea5df33a44/ea61a27b-a46e-9161-565c-beea5df33a44/ea61a27b-a46e-9161-565c-beea5df33a44/ea61a27b-a46e-9161-565c-beea5df33a44/934069f8-4bc3-ff78-317c-585569527563/934069f8-4bc3-ff78-317c-585569527563/934069f8-4bc3-ff78-317c-585569527563/934069f8-4bc3-ff78-317c-585569527563/934069f8-4bc3-ff78-317c-585569527563/934069f8-4bc3-ff78-317c-585569527563]","[Body/Neck/Head/Ears Upright/Ears Drooped/Tail/LF Back/LF Front/LF Stand/RF Back/RF Front/RF Stand/LR Back/LR Front/LR Stand/RR Back/RR Front/RR Stand]");
		red.setOccurence(35);
		red.setTraittype(TraitType.Breeding);
		
		PeltColor cream= new PeltColor("Cream", solid, "[c22d52e4-a10a-3fe1-b418-c63acac8479c/5fac529d-6e60-fc48-bb84-add60b15f7d3/92f246b0-f002-b95e-466d-814da1472b69/af17e8df-f547-e049-9c80-7e3b5af45142/af17e8df-f547-e049-9c80-7e3b5af45142/e49a6682-12f0-56c0-dd92-1b6dd8445e93/c8c9c67a-94d0-6d1e-6003-691e3a2b98f8/c8c9c67a-94d0-6d1e-6003-691e3a2b98f8/c8c9c67a-94d0-6d1e-6003-691e3a2b98f8/c8c9c67a-94d0-6d1e-6003-691e3a2b98f8/c8c9c67a-94d0-6d1e-6003-691e3a2b98f8/c8c9c67a-94d0-6d1e-6003-691e3a2b98f8/1c4f839d-3899-858e-40c3-5d14f9075bed/1c4f839d-3899-858e-40c3-5d14f9075bed/1c4f839d-3899-858e-40c3-5d14f9075bed/1c4f839d-3899-858e-40c3-5d14f9075bed/1c4f839d-3899-858e-40c3-5d14f9075bed/1c4f839d-3899-858e-40c3-5d14f9075bed]","[Body/Neck/Head/Ears Upright/Ears Drooped/Tail/LF Back/LF Front/LF Stand/RF Back/RF Front/RF Stand/LR Back/LR Front/LR Stand/RR Back/RR Front/RR Stand]");
		cream.setOccurence(35);
		cream.setTraittype(TraitType.Breeding);
		
		PeltColor chocolate = new PeltColor("Chocolate", solid, "[ad5415b5-0be1-8ecf-7774-622f2c1d7cf8/f93f08f0-38f4-d2e2-514f-1a0744abd779/f083970f-c348-adf2-7e42-4d49116ddcc6/26732f04-5464-7e85-c7e9-a3c7cbce1746/26732f04-5464-7e85-c7e9-a3c7cbce1746/3ea5cda3-806f-4224-257c-4eb8974c23fc/05b1016c-0c91-8fa5-d588-4cdeafb92997/05b1016c-0c91-8fa5-d588-4cdeafb92997/05b1016c-0c91-8fa5-d588-4cdeafb92997/05b1016c-0c91-8fa5-d588-4cdeafb92997/05b1016c-0c91-8fa5-d588-4cdeafb92997/05b1016c-0c91-8fa5-d588-4cdeafb92997/9c0238d2-73ef-5fea-5e51-105e972d5354/9c0238d2-73ef-5fea-5e51-105e972d5354/9c0238d2-73ef-5fea-5e51-105e972d5354/9c0238d2-73ef-5fea-5e51-105e972d5354/9c0238d2-73ef-5fea-5e51-105e972d5354/9c0238d2-73ef-5fea-5e51-105e972d5354]","[Body/Neck/Head/Ears Upright/Ears Drooped/Tail/LF Back/LF Front/LF Stand/RF Back/RF Front/RF Stand/LR Back/LR Front/LR Stand/RR Back/RR Front/RR Stand]");
		chocolate.setOccurence(35);
		chocolate.setTraittype(TraitType.Breeding);
		
		Body normalBody = new Body("Normal","","");
		normalBody.setParameteroverride(false);
		normalBody.setOccurence(100);
		normalBody.setTraittype(TraitType.Starter);
		
		Ear normalEar = new Ear("Normal","","");
		normalEar.setParameteroverride(false);
		normalEar.setOccurence(100);
		normalEar.setTraittype(TraitType.Starter);
		
		Tail normalTail = new Tail("Normal","","");
		normalTail.setParameteroverride(false);
		normalTail.setOccurence(100);
		normalTail.setTraittype(TraitType.Starter);
		
		Whiskers normalWhiskers = new Whiskers("Normal","[ed73c28c-f391-3bfe-f114-72dc29cd69d5]","[Whiskers]");
		normalWhiskers.setOccurence(100);
		normalWhiskers.setTraittype(TraitType.Starter);
		
		EyeShape normalEyeShape = new EyeShape("Normal");
		normalEyeShape.setOccurence(100);
		normalEyeShape.setTraittype(TraitType.Starter);
		
		EyeColor blueEyeColor = new EyeColor("Blue");
		blueEyeColor.setOccurence(100);
		blueEyeColor.setTraittype(TraitType.Starter);
		
		EyeColor amberEyeColor = new EyeColor("Amber");
		amberEyeColor.setOccurence(100);
		amberEyeColor.setTraittype(TraitType.Starter);
		
		EyeColor greenEyeColor = new EyeColor("Green");
		greenEyeColor.setOccurence(100);
		greenEyeColor.setTraittype(TraitType.Starter);
		
		EyeColor lbEyeColor = new EyeColor("Light Blue");
		lbEyeColor.setOccurence(100);
		lbEyeColor.setTraittype(TraitType.Starter);
		
		EyeColor pinkEyeColor = new EyeColor("Pink");
		pinkEyeColor.setOccurence(100);
		pinkEyeColor.setTraittype(TraitType.Starter);
		
		EyeColor redEyeColor = new EyeColor("Red");
		redEyeColor.setOccurence(100);
		redEyeColor.setTraittype(TraitType.Starter);
		
		EyeColor turEyeColor = new EyeColor("Turqouise");
		turEyeColor.setOccurence(100);
		turEyeColor.setTraittype(TraitType.Starter);
		
		PupilShape slit = new PupilShape("Slit");
		slit.setOccurence(100);
		slit.setTraittype(TraitType.Starter);
		
		Eyes eyesamber = new Eyes(amberEyeColor,normalEyeShape,slit,"[d76d0512-0025-2eef-9fa6-3a7c95837b21]","[Eyes]");
		Eyes eyesblue = new Eyes(blueEyeColor,normalEyeShape,slit,"[d416293c-8c5b-c823-e5db-fe60f5117eb5]","[Eyes]");
		Eyes eyesgreen = new Eyes(greenEyeColor,normalEyeShape,slit,"[df489c01-63fd-7dc5-811f-8ba9484fadd0]","[Eyes]");
		Eyes eyeslightblue = new Eyes(lbEyeColor,normalEyeShape,slit,"[d507e10c-82c2-a381-363b-0be0ccedc55d]","[Eyes]");
		Eyes eyespink = new Eyes(pinkEyeColor,normalEyeShape,slit,"[89da6b39-0577-283e-1669-16cf3a8f6a26]","[Eyes]");
		Eyes eyesred = new Eyes(redEyeColor,normalEyeShape,slit,"[6b196ef5-8bf9-2bac-3439-c293faff4ea8]","[Eyes]");
		Eyes eyestur = new Eyes(turEyeColor,normalEyeShape,slit,"[3389fc9b-22e9-89e5-b536-16e110c41806]","[Eyes]");
		
		em.persist(grey);
		em.persist(black);
		em.persist(white);
		em.persist(cream);
		em.persist(red);
		em.persist(chocolate);
		
		
		em.persist(normalBody);
		em.persist(normalEar);
		em.persist(normalTail);
		em.persist(normalEyeShape);
		em.persist(normalWhiskers);
		em.persist(slit);
		
		em.persist(blueEyeColor);
		em.persist(amberEyeColor);
		em.persist(greenEyeColor);
		em.persist(lbEyeColor);
		em.persist(pinkEyeColor);
		em.persist(redEyeColor);
		em.persist(turEyeColor);
		
		em.persist(eyesamber);
		em.persist(eyesblue);
		em.persist(eyesgreen);
		em.persist(eyeslightblue);
		em.persist(eyespink);
		em.persist(eyesred);
		em.persist(eyestur);
	}

	private void createCatTypes()
	{
		CatType wild = new CatType("Wild Cat", "[a3c8210a-2f3b-4b43-108b-bad691194a8d/0f34966b-a1a1-f16b-f2e8-a8a478b047d2/2a811816-9df0-186f-b2ae-e8aa7662d3e8/d5a2b0ed-32c4-e3a8-f2a0-1212b9fedd37/67d04c29-fec5-9b35-f540-dae5ebd7b998/31736cfb-a30d-ad7d-d97b-55a498f54129/c5556498-47e7-b125-92ab-72af8e3d822c/2fc08003-3eb2-f83c-33b4-2fc75af6c475/9af7a18b-e93b-624b-9f3d-a56c3ca57869/8fc0556e-0c71-8db5-9b85-42f4ecfa0e67/2fc08003-3eb2-f83c-33b4-2fc75af6c475/9af7a18b-e93b-624b-9f3d-a56c3ca57869/8fc0556e-0c71-8db5-9b85-42f4ecfa0e67/ba8fe4ed-b8fc-903e-94ee-b644387944b4/23138050-d2de-3cbd-dfe5-3149cea395b8/f8495f3a-0f60-60a1-96d4-36ad2ec2e915/ba8fe4ed-b8fc-903e-94ee-b644387944b4/23138050-d2de-3cbd-dfe5-3149cea395b8/f8495f3a-0f60-60a1-96d4-36ad2ec2e915/dde5e5a7-9cde-8a61-2ae4-5328fdf20237]","[Body/Neck/Head/Eyes/Ears Upright/Ears Drooped/Tail/LF Back/LF Front/LF Stand/RF Back/RF Front/RF Stand/LR Back/LR Front/LR Stand/RR Back/RR Front/RR Stand/Whiskers]");
		wild.setPosition_gen_3("[<0.00000,0.00000,0.00000>/<0.00000,-0.31819,0.19886>/<0.00000,-0.39276,0.29328>/<0.00000,-0.45868,0.33802>/<0.00000,-0.33808,0.37781>/<0.00000,-0.33808,0.35791>/<0.00000,0.57669,0.00989>/<0.10545,-0.20285,-0.14343>/<0.10545,-0.34604,-0.07806>/<0.08954,-0.25057,-0.14331>/<-0.10547,-0.20285,-0.14343>/<-0.10547,-0.34604,-0.07806>/<-0.08954,-0.25057,-0.14331>/<0.08954,0.35397,-0.14343>/<0.08954,0.21078,-0.08599>/<0.07364,0.25056,-0.13635>/<-0.08954,0.35397,-0.14343>/<-0.08954,0.21078,-0.08599>/<-0.07364,0.25056,-0.13635>/<0.00000,-0.51464,0.21094>];");
		wild.setPosition_gen_2("[<0.00000,0.00000,0.00000>/<0.00000,-0.25178,0.15741>/<0.00000,-0.31080,0.23206>/<0.00000,-0.36296,0.26746>/<0.00000,-0.26753,0.29895>/<0.00000,-0.26753,0.28321>/<0.00000,0.45633,0.00782>/<0.08340,-0.16047,-0.11346>/<0.08340,-0.27377,-0.06182>/<0.07081,-0.19828,-0.11340>/<-0.08348,-0.16055,-0.11346>/<-0.08348,-0.27377,-0.06182>/<-0.07082,-0.19828,-0.11340>/<0.07081,0.28011,-0.11346>/<0.07081,0.16680,-0.06805>/<0.05824,0.19829,-0.10784>/<-0.07082,0.28011,-0.11346>/<-0.07082,0.16680,-0.06805>/<-0.05824,0.19829,-0.10784>/<0.00000,-0.40718,0.16699>];");
		wild.setPosition_gen_1("[<0.00000,0.00000,0.00000>/<0.00000,-0.19067,0.11920>/<0.00000,-0.23536,0.17578>/<0.00000,-0.27485,0.20251>/<0.00000,-0.20259,0.22638>/<0.00000,-0.20259,0.21448>/<0.00000,0.34557,0.00592>/<0.06315,-0.12152,-0.08594>/<0.06315,-0.20733,-0.04682>/<0.05363,-0.15016,-0.08588>/<-0.06322,-0.12158,-0.08594>/<-0.06322,-0.20733,-0.04682>/<-0.05364,-0.15016,-0.08588>/<0.05363,0.21213,-0.08594>/<0.05363,0.12632,-0.05152>/<0.04409,0.15017,-0.08167>/<-0.05364,0.21213,-0.08594>/<-0.05364,0.12632,-0.05152>/<-0.04410,0.15017,-0.08167>/<0.00000,-0.32544,0.13360>];");
		wild.setSize_gen_3("[<0.45739,0.83523,0.31818>/<0.19886,0.43757,0.20880>/<0.27841,0.29829,0.29829>/<0.20682,0.17440,0.29547>/<0.23867,0.33807,0.09943>/<0.17896,0.41761,0.13921>/<0.11932,0.51705,0.27839>/<0.16723,0.54085,0.31818>/<0.15908,0.41375,0.44545>/<0.16723,0.54289,0.25454>/<0.16723,0.54085,0.31818>/<0.15908,0.41375,0.44545>/<0.16723,0.54289,0.25454>/<0.15909,0.54085,0.38182>/<0.15908,0.49318,0.38181>/<0.16723,0.55697,0.30227>/<0.15909,0.54085,0.38182>/<0.15908,0.49318,0.38181>/<0.16723,0.55697,0.30227>/<0.33545,0.13609,0.03946>];");
		wild.setSize_gen_2("[<0.36192,0.66089,0.25177>/<0.15735,0.34624,0.16522>/<0.22030,0.23603,0.23603>/<0.16365,0.13800,0.23380>/<0.18885,0.26751,0.07868>/<0.14161,0.33044,0.11015>/<0.09441,0.40913,0.22028>/<0.13232,0.42796,0.25177>/<0.12588,0.32739,0.35247>/<0.13232,0.42957,0.20141>/<0.13232,0.42796,0.25177>/<0.12588,0.32739,0.35247>/<0.13232,0.42957,0.20141>/<0.12588,0.42796,0.30212>/<0.12588,0.39024,0.30212>/<0.13232,0.44071,0.23918>/<0.12588,0.42796,0.30212>/<0.12588,0.39024,0.30212>/<0.13232,0.44071,0.23918>/<0.26543,0.10688,0.03122>];");
		wild.setSize_gen_1("[<0.27408,0.50049,0.19066>/<0.11916,0.26220,0.12512>/<0.16683,0.17874,0.17874>/<0.12393,0.10450,0.17705>/<0.14302,0.20258,0.05958>/<0.10724,0.25024,0.08342>/<0.07150,0.30983,0.16682>/<0.10021,0.32409,0.19066>/<0.09532,0.24793,0.26692>/<0.10021,0.32531,0.15253>/<0.10021,0.32409,0.19066>/<0.09532,0.24793,0.26692>/<0.10021,0.32531,0.15253>/<0.09533,0.32409,0.22879>/<0.09532,0.29552,0.22879>/<0.10021,0.33375,0.18113>/<0.09533,0.32409,0.22879>/<0.09532,0.29552,0.22879>/<0.10021,0.33375,0.18113>/<0.21214,0.08282,0.02495>];");
		wild.setRotation("[<0.00000,90.00000,180.00000>/<90.00000,0.00000,0.00000>/<0.00000,90.00000,180.00000>/<0.00000,90.00000,180.00000>/<90.00000,0.00000,90.00000>/<90.00000,0.00000,90.00000>/<0.00000,0.00000,0.00000>/<90.00000,0.00000,0.00000>/<90.00000,0.00000,0.00000>/<90.00000,0.00000,0.00000>/<90.00000,0.00000,0.00000>/<90.00000,0.00000,0.00000>/<90.00000,0.00000,0.00000>/<90.00000,0.00000,0.00000>/<90.00000,0.00000,0.00000>/<90.00000,0.00000,0.00000>/<90.00000,0.00000,0.00000>/<90.00000,0.00000,0.00000>/<90.00000,0.00000,0.00000>/<90.00000,0.00000,0.00000>]");
		wild.setMirror("[0/0/0/0/0/0/0/1/1/1/0/0/0/1/1/1/0/0/0/0]");
		wild.setStitching("[0/0/0/1/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/1]");
		wild.setOccurence(100);
		wild.setHeight1(0.13147);
		wild.setHeight2(0.19934);
		wild.setHeight3(0.29510);
		em.persist(wild);
		
		CatType big = new CatType("Big Cat", "[0892a05d-f94a-2742-d39d-9774b054563d/0f34966b-a1a1-f16b-f2e8-a8a478b047d2/ffe9cc8b-c13c-74f2-cb14-30b397e0c7a4/5b223ebf-3feb-8d2f-6204-f2434bdaabd0/4bb53755-bf56-9fee-907d-13abe6067bf8/55ad982a-50c8-46ea-d3d5-5a32dccaba3a/b6d4cd44-660c-dca8-310b-6035e925e715/ec727964-61d2-9ba4-65c3-70a7e669dbce/8d3a3ab5-353c-6638-ac03-02d8512e1a85/66e918fe-9bb7-fcad-13c6-cd868df9503b/ec727964-61d2-9ba4-65c3-70a7e669dbce/8d3a3ab5-353c-6638-ac03-02d8512e1a85/66e918fe-9bb7-fcad-13c6-cd868df9503b/68f73454-20e6-f4ed-30f8-a9b692440444/53e87109-0cd0-f5b4-713d-e90143cfcd06/2dbab006-1acb-e9e3-bf93-e098fb83d1b6/68f73454-20e6-f4ed-30f8-a9b692440444/53e87109-0cd0-f5b4-713d-e90143cfcd06/2dbab006-1acb-e9e3-bf93-e098fb83d1b6/dde5e5a7-9cde-8a61-2ae4-5328fdf20237]","[Body/Neck/Head/Eyes/Ears Upright/Ears Drooped/Tail/LF Back/LF Front/LF Stand/RF Back/RF Front/RF Stand/LR Back/LR Front/LR Stand/RR Back/RR Front/RR Stand/Whiskers]");
		big.setPosition_gen_3("[<0.00000,0.00000,0.00000>/<0.00000,-0.67324,0.42078>/<0.00000,-0.83101,0.62048>/<0.00000,-0.97049,0.71521>/<0.00000,-0.71530,0.79944>/<0.00000,-0.71530,0.75726>/<0.00000,1.22017,0.02093>/<0.22311,-0.42922,-0.30341>/<0.22311,-0.73218,-0.16516>/<0.18947,-0.53017,-0.30322>/<-0.22312,-0.42922,-0.30341>/<-0.22312,-0.73218,-0.16516>/<-0.18947,-0.53017,-0.30322>/<0.18947,0.74895,-0.30347>/<0.18947,0.44598,-0.18201>/<0.15581,0.53013,-0.28845>/<-0.18947,0.74895,-0.30347>/<-0.18947,0.44598,-0.18201>/<-0.15581,0.53013,-0.28845>/<0.00000,-1.09271,0.44751>];");
		big.setPosition_gen_2("[<0.00000,0.00000,0.00000>/<0.00000,-0.55214,0.34510>/<0.00000,-0.68153,0.50885>/<0.00000,-0.79593,0.58655>/<0.00000,-0.58664,0.65564>/<0.00000,-0.58664,0.62104>/<0.00000,1.00070,0.01715>/<0.18298,-0.35202,-0.24884>/<0.18298,-0.60048,-0.13543>/<0.15539,-0.43481,-0.24865>/<-0.18299,-0.35202,-0.24884>/<-0.18299,-0.60048,-0.13543>/<-0.15540,-0.43481,-0.24865>/<0.15539,0.61423,-0.24890>/<0.15539,0.36576,-0.14929>/<0.12777,0.43477,-0.23657>/<-0.15540,0.61423,-0.24890>/<-0.15540,0.36576,-0.14929>/<-0.12778,0.43477,-0.23657>/<0.00000,-0.80699,0.33209>];");
		big.setPosition_gen_1("[<0.00000,0.00000,0.00000>/<0.00000,-0.36115,0.22577>/<0.00000,-0.44580,0.33289>/<0.00000,-0.52065,0.38367>/<0.00000,-0.38373,0.42883>/<0.00000,-0.38373,0.40625>/<0.00000,0.65458,0.01123>/<0.11969,-0.23026,-0.16278>/<0.11969,-0.39279,-0.08862>/<0.10166,-0.28444,-0.16266>/<-0.11969,-0.23026,-0.16278>/<-0.11969,-0.39279,-0.08862>/<-0.10165,-0.28444,-0.16266>/<0.10166,0.40181,-0.16284>/<0.10166,0.23926,-0.09765>/<0.08359,0.28438,-0.15478>/<-0.10165,0.40181,-0.16284>/<-0.10165,0.23926,-0.09765>/<-0.08359,0.28438,-0.15478>/<0.00000,-0.58619,0.23963>];");
		big.setSize_gen_3("[<0.96776,1.76721,0.67323>/<0.42077,0.92584,0.44180>/<0.58907,0.63113,0.63113>/<0.43761,0.36900,0.62518>/<0.50499,0.71530,0.21038>/<0.37865,0.88361,0.29454>/<0.25246,1.09399,0.58903>/<0.33661,1.14436,0.67322>/<0.33659,0.87542,0.94251>/<0.35383,1.14867,0.53857>/<0.33661,1.14436,0.67322>/<0.33659,0.87542,0.94251>/<0.35383,1.14867,0.53857>/<0.33661,1.14436,0.80786>/<0.33659,1.04348,0.80786>/<0.35383,1.17846,0.63955>/<0.33661,1.14436,0.80786>/<0.33659,1.04348,0.80786>/<0.35383,1.17846,0.63955>/<0.57223,0.22852,0.06732>];");
		big.setSize_gen_2("[<0.79369,1.44934,0.55213>/<0.34509,0.75931,0.36233>/<0.48311,0.51761,0.51761>/<0.35890,0.30263,0.51273>/<0.41416,0.58664,0.17254>/<0.31054,0.72467,0.24156>/<0.20705,0.89721,0.48308>/<0.27606,0.93852,0.55213>/<0.27605,0.71796,0.77298>/<0.29019,0.94206,0.44170>/<0.27606,0.93852,0.55213>/<0.27605,0.71796,0.77298>/<0.29019,0.94206,0.44170>/<0.27606,0.93852,0.66255>/<0.27605,0.85579,0.66255>/<0.29019,0.96649,0.52451>/<0.27606,0.93852,0.66255>/<0.27605,0.85579,0.66255>/<0.29019,0.96649,0.52451>/<0.42261,0.18647,0.04972>];");
		big.setSize_gen_1("[<0.51917,0.94804,0.36116>/<0.22573,0.49668,0.23701>/<0.31601,0.33858,0.33858>/<0.23476,0.19796,0.33539>/<0.27091,0.38373,0.11286>/<0.20313,0.47402,0.15801>/<0.13544,0.58689,0.31599>/<0.18058,0.61391,0.36116>/<0.18057,0.46963,0.50562>/<0.18982,0.61622,0.28892>/<0.18058,0.61391,0.36116>/<0.18057,0.46963,0.50562>/<0.18982,0.61622,0.28892>/<0.18058,0.61391,0.43339>/<0.18057,0.55979,0.43339>/<0.18982,0.63220,0.34310>/<0.18058,0.61391,0.43339>/<0.18057,0.55979,0.43339>/<0.18982,0.63220,0.34310>/<0.30698,0.12747,0.03611>];");
		big.setRotation("[<0.00000,90.00000,180.00000>/<90.00000,0.00000,0.00000>/<0.00000,90.00000,180.00000>/<0.00000,90.00000,180.00000>/<90.00000,0.00000,90.00000>/<90.00000,0.00000,90.00000>/<0.00000,0.00000,0.00000>/<90.00000,0.00000,0.00000>/<90.00000,0.00000,0.00000>/<90.00000,0.00000,0.00000>/<90.00000,0.00000,0.00000>/<90.00000,0.00000,0.00000>/<90.00000,0.00000,0.00000>/<90.00000,0.00000,0.00000>/<90.00000,0.00000,0.00000>/<90.00000,0.00000,0.00000>/<90.00000,0.00000,0.00000>/<90.00000,0.00000,0.00000>/<90.00000,0.00000,0.00000>/<90.00000,0.00000,0.00000>]");
		big.setMirror("[0/0/0/0/0/0/0/1/1/1/0/0/0/1/1/1/0/0/0/0]");
		big.setStitching("[0/0/0/1/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/1]");
		big.setHeight1(0.34375);
		big.setHeight2(0.51672);
		big.setHeight3(0.75140);
		big.setOccurence(100);
		em.persist(big);
		
		CatType feisty = new CatType("Feisty", "[0892a05d-f94a-2742-d39d-9774b054563d/0f34966b-a1a1-f16b-f2e8-a8a478b047d2/cc33e71e-6e41-649a-c76d-d3d734732ebf/06142c12-4219-4481-6752-ec093b78c3f3/4bb53755-bf56-9fee-907d-13abe6067bf8/55ad982a-50c8-46ea-d3d5-5a32dccaba3a/b6d4cd44-660c-dca8-310b-6035e925e715/ec727964-61d2-9ba4-65c3-70a7e669dbce/8d3a3ab5-353c-6638-ac03-02d8512e1a85/66e918fe-9bb7-fcad-13c6-cd868df9503b/ec727964-61d2-9ba4-65c3-70a7e669dbce/8d3a3ab5-353c-6638-ac03-02d8512e1a85/66e918fe-9bb7-fcad-13c6-cd868df9503b/68f73454-20e6-f4ed-30f8-a9b692440444/53e87109-0cd0-f5b4-713d-e90143cfcd06/2dbab006-1acb-e9e3-bf93-e098fb83d1b6/68f73454-20e6-f4ed-30f8-a9b692440444/53e87109-0cd0-f5b4-713d-e90143cfcd06/2dbab006-1acb-e9e3-bf93-e098fb83d1b6/dde5e5a7-9cde-8a61-2ae4-5328fdf20237]","[Body/Neck/Head/Eyes/Ears Upright/Ears Drooped/Tail/LF Back/LF Front/LF Stand/RF Back/RF Front/RF Stand/LR Back/LR Front/LR Stand/RR Back/RR Front/RR Stand/Whiskers]");
		feisty.setPosition_gen_3("[<0.00000,0.00000,0.00000>/<0.00000,-0.16490,0.10308>/<0.00000,-0.20355,0.15197>/<0.00000,-0.23772,0.17517>/<0.00000,-0.17520,0.19580>/<0.00000,-0.17520,0.18548>/<0.00000,0.29886,0.00512>/<0.05465,-0.10513,-0.07434>/<0.05465,-0.17934,-0.04047>/<0.04641,-0.12985,-0.07428>/<-0.05464,-0.10513,-0.07434>/<-0.05464,-0.17934,-0.04047>/<-0.04640,-0.12985,-0.07428>/<0.04641,0.18344,-0.07434>/<0.04641,0.10924,-0.04462>/<0.03817,0.12985,-0.07068>/<-0.04640,0.18344,-0.07434>/<-0.04640,0.10924,-0.04462>/<-0.03816,0.12985,-0.07068>/<0.00000,-0.26700,0.11133>];");
		feisty.setPosition_gen_2("[<0.00000,0.00000,0.00000>/<0.00000,-0.13579,0.08490>/<0.00000,-0.16766,0.12518>/<0.00000,-0.19574,0.14428>/<0.00000,-0.14427,0.16125>/<0.00000,-0.14427,0.15277>/<0.00000,0.24614,0.00421>/<0.04505,-0.08655,-0.06116>/<0.04505,-0.14764,-0.03333>/<0.03821,-0.10689,-0.06116>/<-0.04497,-0.08655,-0.06116>/<-0.04497,-0.14764,-0.03333>/<-0.03821,-0.10696,-0.06116>/<0.03821,0.15103,-0.06116>/<0.03821,0.08992,-0.03675>/<0.03147,0.10697,-0.05823>/<-0.03821,0.15103,-0.06116>/<-0.03821,0.08992,-0.03675>/<-0.03146,0.10697,-0.05823>/<0.00000,-0.21986,0.09180>];");
		feisty.setPosition_gen_1("[<0.00000,0.00000,0.00000>/<-0.00004,-0.10000,0.06250>/<0.00000,-0.12347,0.09216>/<0.00000,-0.14415,0.10626>/<0.00000,-0.10625,0.11871>/<0.00000,-0.10625,0.11248>/<0.00000,0.18126,0.00311>/<0.03317,-0.06373,-0.04505>/<0.03317,-0.10873,-0.02454>/<0.02813,-0.07872,-0.04505>/<-0.03312,-0.06373,-0.04505>/<-0.03312,-0.10873,-0.02454>/<-0.02814,-0.07878,-0.04505>/<0.02813,0.11122,-0.04505>/<0.02813,0.06622,-0.02704>/<0.02316,0.07878,-0.04285>/<-0.02814,0.11122,-0.04505>/<-0.02814,0.06622,-0.02704>/<-0.02317,0.07878,-0.04285>/<0.00000,-0.16191,0.06763>];");
		feisty.setSize_gen_3("[<0.23704,0.43285,0.16490>/<0.10306,0.22677,0.10821>/<0.14428,0.15459,0.15459>/<0.10719,0.09038,0.15313>/<0.12369,0.17520,0.05153>/<0.09274,0.21643,0.07214>/<0.06184,0.26796,0.14427>/<0.08245,0.28029,0.16490>/<0.08244,0.21442,0.23085>/<0.08667,0.28135,0.13191>/<0.08245,0.28029,0.16490>/<0.08244,0.21442,0.23085>/<0.08667,0.28135,0.13191>/<0.08245,0.28029,0.19787>/<0.08667,0.28865,0.15665>/<0.08667,0.28865,0.15665>/<0.08245,0.28029,0.19787>/<0.08667,0.28865,0.15665>/<0.08667,0.28865,0.15665>/<0.14016,0.05174,0.01649>];");
		feisty.setSize_gen_2("[<0.19520,0.35644,0.13579>/<0.08487,0.18674,0.08911>/<0.11881,0.12730,0.12730>/<0.08827,0.07443,0.12610>/<0.10186,0.14427,0.04243>/<0.07637,0.17823,0.05941>/<0.05092,0.22066,0.11880>/<0.06790,0.23081,0.13579>/<0.06789,0.17657,0.19010>/<0.07137,0.23169,0.10863>/<0.06790,0.23081,0.13579>/<0.06789,0.17657,0.19010>/<0.07137,0.23169,0.10863>/<0.06790,0.23081,0.16294>/<0.07137,0.23770,0.12900>/<0.07137,0.23770,0.12900>/<0.06790,0.23081,0.16294>/<0.07137,0.23770,0.12900>/<0.07137,0.23770,0.12900>/<0.11542,0.04261,0.01358>];");
		feisty.setSize_gen_1("[<0.14375,0.26249,0.10000>/<0.06250,0.13752,0.06562>/<0.08750,0.09375,0.09375>/<0.06500,0.05481,0.09286>/<0.07501,0.10625,0.03125>/<0.05624,0.13125,0.04375>/<0.03750,0.16250,0.08749>/<0.05000,0.16998,0.10000>/<0.04999,0.13003,0.13999>/<0.05256,0.17062,0.07999>/<0.05000,0.16998,0.10000>/<0.04999,0.13003,0.13999>/<0.05256,0.17062,0.07999>/<0.05000,0.16998,0.11999>/<0.05256,0.17505,0.09500>/<0.05256,0.17505,0.09500>/<0.05000,0.16998,0.11999>/<0.05256,0.17505,0.09500>/<0.05256,0.17505,0.09500>/<0.08500,0.02991,0.01000>];");
		feisty.setRotation("[<0.00000,90.00000,180.00000>/<90.00000,0.00000,0.00000>/<0.00000,90.00000,180.00000>/<0.00000,90.00000,180.00000>/<90.00000,0.00000,90.00000>/<90.00000,0.00000,90.00000>/<0.00000,0.00000,0.00000>/<90.00000,0.00000,0.00000>/<90.00000,0.00000,0.00000>/<90.00000,0.00000,0.00000>/<90.00000,0.00000,0.00000>/<90.00000,0.00000,0.00000>/<90.00000,0.00000,0.00000>/<90.00000,0.00000,0.00000>/<90.00000,0.00000,0.00000>/<90.00000,0.00000,0.00000>/<90.00000,0.00000,0.00000>/<90.00000,0.00000,0.00000>/<90.00000,0.00000,0.00000>/<90.00000,0.00000,0.00000>]");
		feisty.setMirror("[0/0/0/0/0/0/0/1/1/1/0/0/0/1/1/1/0/0/0/0]");
		feisty.setStitching("[0/0/0/1/0/0/0/0/0/0/0/0/0/0/0/0/0/0/0/1]");
		feisty.setOccurence(100);
		feisty.setHeight1(-0.00134);
		feisty.setHeight2(0.04925);
		feisty.setHeight1(0.08752);
		em.persist(feisty);
	}
}
