package com.feistyfelinesbreedables.portal.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;
@Component
public class Startup implements ApplicationListener<ContextRefreshedEvent> {
	@Autowired
	private CreationService service;
	
	@Override
	public void onApplicationEvent(final ContextRefreshedEvent event) {
		service.createStartingData();
	}
}
