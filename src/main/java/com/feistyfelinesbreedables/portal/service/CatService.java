package com.feistyfelinesbreedables.portal.service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Random;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.feistyfelinesbreedables.portal.engine.CatGenerationType;
import com.feistyfelinesbreedables.portal.entity.Body;
import com.feistyfelinesbreedables.portal.entity.Cat;
import com.feistyfelinesbreedables.portal.entity.CatType;
import com.feistyfelinesbreedables.portal.entity.Ear;
import com.feistyfelinesbreedables.portal.entity.EyeColor;
import com.feistyfelinesbreedables.portal.entity.EyeShape;
import com.feistyfelinesbreedables.portal.entity.Owner;
import com.feistyfelinesbreedables.portal.entity.Pelt;
import com.feistyfelinesbreedables.portal.entity.PeltColor;
import com.feistyfelinesbreedables.portal.entity.PupilShape;
import com.feistyfelinesbreedables.portal.entity.Tail;
import com.feistyfelinesbreedables.portal.repository.CatRepository;



@Service
@Transactional
public class CatService {
	Random random = new Random(System.currentTimeMillis());
	
	
	
	public Cat generateBaby(Cat father, Cat mother)
	{
		return null;
	}

	public Cat getCatAndParents(long id)
	{
		List<Cat> cats = em.createNamedQuery("getCatById",Cat.class).setParameter("id", id).getResultList();
		if(cats.size() > 0)
		{
			Cat cat = cats.get(0);
			Hibernate.initialize(cat.getFather());
			Hibernate.initialize(cat.getMother());
			return cat;
		}
		else 
		{
			return null;
		}
	}
	public Cat getCatDetails(long id)
	{
		List<Cat> cats = em.createNamedQuery("getCatById",Cat.class).setParameter("id", id).getResultList();
		if(cats.size() > 0)
		{
			Cat cat = cats.get(0);
			Hibernate.initialize(cat.getFather());
			Hibernate.initialize(cat.getPregnantwith());
			Hibernate.initialize(cat.getBody());
			Hibernate.initialize(cat.getPelt());
			Hibernate.initialize(cat.getPeltcolor());
			Hibernate.initialize(cat.getTail());
			Hibernate.initialize(cat.getWhiskers());
			Hibernate.initialize(cat.getPupilshape());
			Hibernate.initialize(cat.getEyeshape());
			Hibernate.initialize(cat.getEyecolor());
			Hibernate.initialize(cat.getEar());
			Hibernate.initialize(cat.getMother());
			return cat;
		}
		else 
		{
			return null;
		}
	}
	
	
	@Autowired
	private EntityManager em;
	public Cat generateOrReturnBasket(int generationId, String basketid, String ownerid)
	{
		Cat cat = null;
		Owner o = Owner.getOrCreateOwner(ownerid, em);


		try
		{
			cat = (Cat)em.createNamedQuery("getCatByBasketId").setParameter("basketid", basketid).getResultList().get(0);
			cat.setOwner(o);
			em.persist(cat);

			return cat;
		}
		catch(Exception ex)
		{	
			if(generationId >= 0)
			{
				CatGenerationType type = CatGenerationType.values()[generationId];
				cat = new Cat(type,basketid, o,em);
				em.persist(cat);
				return cat;
			}
			else
				return null;
		}
		
	}
}
