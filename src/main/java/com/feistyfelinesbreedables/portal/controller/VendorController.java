package com.feistyfelinesbreedables.portal.controller;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.feistyfelinesbreedables.portal.entity.Vendor;
import com.feistyfelinesbreedables.portal.helpers.ValidationHelper;
import com.feistyfelinesbreedables.portal.repository.VendorRepository;

import java.io.IOException;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;


@Controller
@RequestMapping("/Vendor")
@Transactional
public class VendorController 
{
	@Autowired
	private VendorRepository repository;
	
	
@RequestMapping("/getVendor")
	public void getVendor(@RequestParam(value="id", required=true) long id,@RequestHeader("Script-Key") String scriptKey,@RequestHeader("X-SecondLife-Owner-Key") String key,HttpServletResponse response) throws IOException 
	{
		if(ValidationHelper.ValidateVendorOwner(scriptKey, key))
		{
				Vendor vendor = repository.findOne(id);
				//Hibernate.initialize(vendor.getVendorlines());
				String v = vendor.toString();
				response.setStatus(200);
				
				ServletOutputStream outStream = response.getOutputStream();
				response.setContentType("application/xml");
				outStream.print(v);
				outStream.flush();
		    	outStream.close();
		}
		else
		{
			response.sendError(403);
		}
		
		
	}
}