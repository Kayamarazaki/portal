package com.feistyfelinesbreedables.portal.controller;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.EntityManager;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.feistyfelinesbreedables.portal.BreedingSettings;
import com.feistyfelinesbreedables.portal.entity.Body;
import com.feistyfelinesbreedables.portal.entity.Cat;
import com.feistyfelinesbreedables.portal.entity.CatType;
import com.feistyfelinesbreedables.portal.entity.Ear;
import com.feistyfelinesbreedables.portal.entity.EyeColor;
import com.feistyfelinesbreedables.portal.entity.EyeShape;
import com.feistyfelinesbreedables.portal.entity.Owner;
import com.feistyfelinesbreedables.portal.entity.PeltColor;
import com.feistyfelinesbreedables.portal.entity.PupilShape;
import com.feistyfelinesbreedables.portal.entity.Tail;
import com.feistyfelinesbreedables.portal.entity.TraitType;
import com.feistyfelinesbreedables.portal.entity.Whiskers;
import com.feistyfelinesbreedables.portal.helpers.ValidationHelper;
import com.feistyfelinesbreedables.portal.repository.CatRepository;
import com.feistyfelinesbreedables.portal.service.CatService;

@Controller
@RequestMapping("/Cat")
@Transactional
public class CatController {
	@Autowired
	private CatService catService;
	
	@Autowired
	private EntityManager em;
	
	@RequestMapping("/pet")
	public void makePet(@RequestHeader(value="Script-Key", required=true) String scriptKey,@RequestHeader("X-SecondLife-Owner-Key") String ownerid, @RequestParam(value="dbid", required=true) long dbid, HttpServletResponse response) throws IOException
	{
		if(ValidationHelper.Validate(scriptKey))
		{
			Cat cat = catService.getCatDetails(dbid);
			Owner owner = Owner.getOrCreateOwner(ownerid, em);
			if(cat != null && owner != null && !cat.isIspet())
			{
				if(owner.getPetpotions() > 0)
				{
					cat.setIspet(true);
					owner.setPetpotions(owner.getPetpotions() - 1);
					
					int points = 0;
					
					if(cat.getCyclesleft() == 0)
					{
						if(cat.getPelt().getTraittype() == TraitType.LimitedNoPass)
						{
							points += 10;
						}
						else
						{
							points += 3;
						}
					}
					else
					{
						points += 1;
					}
					
					if(cat.getEar().getTraittype() != TraitType.Starter && cat.getEar().getTraittype() != TraitType.Breeding)
					{
						points++;
					}
					
					if(cat.getEyecolor().getTraittype() != TraitType.Starter && cat.getEyecolor().getTraittype() != TraitType.Breeding)
					{
						points++;
					}
					
					if(cat.getEyeshape().getTraittype() != TraitType.Starter && cat.getEyeshape().getTraittype() != TraitType.Breeding)
					{
						points++;
					}
					
					if(cat.getPupilshape().getTraittype() != TraitType.Starter && cat.getPupilshape().getTraittype() != TraitType.Breeding)
					{
						points++;
					}
					
					if(cat.getBody().getTraittype() != TraitType.Starter && cat.getBody().getTraittype() != TraitType.Breeding)
					{
						points++;
					}
					
					if(cat.getTail().getTraittype() != TraitType.Starter && cat.getTail().getTraittype() != TraitType.Breeding)
					{
						points++;
					}
					
					owner.setPawpoints(owner.getPawpoints() + points);
					
					em.persist(cat);
					em.persist(owner);
					response.setStatus(200);
					
				}
				else
				{
					response.sendError(401);
				}
			}
			else
			{
				response.sendError(404);
			}
		}
		else
		{
			response.sendError(403);
		}
	}
	
	@RequestMapping("/custom")
	public void getCustomCatParams(@RequestHeader(value="Script-Key", required=true) String scriptKey, @RequestParam(value="type", required=true) long type, @RequestParam(value="body", required = true) long body, @RequestParam(value="ears", required=true) long ears, @RequestParam(value="eyecolor", required=true) long eyecolor, @RequestParam(value="eyeshape", required=true) long eyeshape, @RequestParam(value="peltcolor", required=true) long peltcolor, @RequestParam(value="pupilshape", required=true) long pupilshape, @RequestParam(value="tail", required=true) long tail, @RequestParam(value="whiskers", required=true) long whiskers, @RequestParam(value="size", required=true) int size, HttpServletResponse response) throws IOException
	{
		if(ValidationHelper.Validate(scriptKey))
		{
			Cat cat = new Cat();
			try
			{
				cat.setCattype(em.createNamedQuery("getCatTypeById", CatType.class).setParameter("id", type).getResultList().get(0));
				cat.setBody(em.createNamedQuery("getBodyById", Body.class).setParameter("id", body).getResultList().get(0));
				cat.setEar(em.createNamedQuery("getEarById", Ear.class).setParameter("id", ears).getResultList().get(0));
				cat.setEyecolor(em.createNamedQuery("getEyeColorById", EyeColor.class).setParameter("id", eyecolor).getResultList().get(0));
				cat.setEyeshape(em.createNamedQuery("getEyeShapeById", EyeShape.class).setParameter("id", eyeshape).getResultList().get(0));
				cat.setPeltcolor(em.createNamedQuery("getPeltColorById", PeltColor.class).setParameter("id", peltcolor).getResultList().get(0));
				cat.setPupilshape(em.createNamedQuery("getPupilShapeById", PupilShape.class).setParameter("id", pupilshape).getResultList().get(0));
				cat.setTail(em.createNamedQuery("getTailById", Tail.class).setParameter("id", tail).getResultList().get(0));
				cat.setWhiskers(em.createNamedQuery("getWhiskersById", Whiskers.class).setParameter("id", whiskers).getResultList().get(0));
				cat.setPelt(cat.getPeltcolor().getPelt());	
				DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
				DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
				Document doc = dBuilder.newDocument();
				
				doc.appendChild(cat.getParametersElement(doc, em,size));
				response.setContentType("application/xml");
				ServletOutputStream outStream = response.getOutputStream();
				
				Transformer transformer = TransformerFactory.newInstance().newTransformer();
		        transformer.setOutputProperty(OutputKeys.INDENT, "yes"); 
		        DOMSource source = new DOMSource(doc);
		        StreamResult result = new StreamResult(outStream);
		        
		        
		        response.setStatus(200);
		        transformer.transform(source, result);
		        
			    outStream.flush();
			    outStream.close();
			
			}
			catch(Exception ex)
			{
				response.sendError(404);
			}
		}
		else
		{
			response.sendError(403);
		}
		
	}
	
	@RequestMapping("/pawpoints")
	public void convertToPawPoints(@RequestHeader(value="Script-Key", required=true) String scriptKey,@RequestHeader("X-SecondLife-Owner-Key") String ownerid,  @RequestParam(value="dbid", required=true) long dbid,@RequestHeader("X-SecondLife-Object-Key") String id,@RequestParam(value="type", required=true) String type,HttpServletResponse response) throws IOException
	{
		if(ValidationHelper.Validate(scriptKey))
		{
			List<Cat> cats = em.createNamedQuery("getCatById", Cat.class).setParameter("id", dbid).getResultList();
			Cat cat = null;
			if(cats.size() > 0)
				cat = cats.get(0);
			else
			{
				if(type.equals("basket"))
				{
					cat = (Cat)em.createNamedQuery("getCatByBasketId").setParameter("basketid", id).getResultList().get(0);
				}
				else if(type.equals("cat"))
				{
					cat = (Cat)em.createNamedQuery("getCatByCatId").setParameter("catid", id).getResultList().get(0);
				}
			}
			int points = 0;
			
			if(cat.getCyclesleft() == 0)
			{
				if(cat.getPelt().getTraittype() == TraitType.LimitedNoPass)
				{
					points += 10;
				}
				else
				{
					points += 3;
				}
			}
			else
			{
				points += 1;
			}
			
			if(cat.getEar().getTraittype() != TraitType.Starter && cat.getEar().getTraittype() != TraitType.Breeding)
			{
				points++;
			}
			
			if(cat.getEyecolor().getTraittype() != TraitType.Starter && cat.getEyecolor().getTraittype() != TraitType.Breeding)
			{
				points++;
			}
			
			if(cat.getEyeshape().getTraittype() != TraitType.Starter && cat.getEyeshape().getTraittype() != TraitType.Breeding)
			{
				points++;
			}
			
			if(cat.getPupilshape().getTraittype() != TraitType.Starter && cat.getPupilshape().getTraittype() != TraitType.Breeding)
			{
				points++;
			}
			
			if(cat.getBody().getTraittype() != TraitType.Starter && cat.getBody().getTraittype() != TraitType.Breeding)
			{
				points++;
			}
			
			if(cat.getTail().getTraittype() != TraitType.Starter && cat.getTail().getTraittype() != TraitType.Breeding)
			{
				points++;
			}
			
			Owner o = Owner.getOrCreateOwner(ownerid, em);
			o.setPawpoints(o.getPawpoints() + points);
			em.persist(o);
			
			response.setStatus(200);
		}
		else
		{
			response.sendError(403);
		}
	}
	

	
	@RequestMapping("/parenttraits")
	public void getParentTraitString(@RequestHeader(value="Script-Key", required=true) String scriptKey,  @RequestParam(value="dbid", required=true) long dbid,@RequestHeader("X-SecondLife-Object-Key") String id, @RequestParam(value="type", required=true) String type,HttpServletResponse response) throws IOException
	{
		if(ValidationHelper.Validate(scriptKey))
		{
			Cat cat = catService.getCatAndParents(dbid);
			
			if(cat != null)
			{
				response.setContentType("text/plain");
				response.setStatus(200);
				ServletOutputStream outStream = response.getOutputStream();
				String traits = cat.getParentTraitString();
				if(traits == null || traits.isEmpty())
					outStream.print("This cat has no parents.");
				else
					outStream.print(traits);
				outStream.flush();
		    	outStream.close();
			}
			else
			{
				response.sendError(404);
			}
		}
		else
		{
			response.sendError(403);
		}
	}
	
	@RequestMapping("/dbid")
	public void dbid(@RequestHeader(value="Script-Key", required=true) String scriptKey,  @RequestParam(value="catid") String uuid, @RequestHeader("X-SecondLife-Owner-Key") String ownerid, @RequestHeader("X-SecondLife-Object-Key") String id, HttpServletResponse response) throws IOException
	{
		if(ValidationHelper.Validate(scriptKey))
		{
			List<Cat> list = em.createNamedQuery("getCatByCatId",Cat.class).setParameter("catid", uuid).getResultList();
			String val = "";
			Cat cat = null;
			if(list.size() > 0)
				cat = list.get(0);
			if(cat != null && cat.getOwner().getId().equals(ownerid))
			{
				val = "Cat"+Long.toString(list.get(0).getId());
			}
			else
			{
				list = em.createNamedQuery("getCatByBasketId",Cat.class).setParameter("basketid", uuid).getResultList();
				if(list.size() > 0)
					cat = list.get(0);
				if(cat != null && cat.getOwner().getId().equals(ownerid))
				{
					val = "Basket"+Long.toString(list.get(0).getId());
				}
				else
				{
					val = "-1";
				}
			}
			response.setContentType("text/plain");
			response.setStatus(200);
			ServletOutputStream outStream = response.getOutputStream();
			outStream.print(val);
			outStream.flush();
	    	outStream.close();
			
		}
		else
		{
			response.sendError(403);
		}
	}
	
	
	@RequestMapping("/heal")
	public void heal(@RequestHeader(value="Script-Key", required=true) String scriptKey,  @RequestParam(value="dbid", required=true) long dbid,@RequestHeader("X-SecondLife-Object-Key") String id, HttpServletResponse response) throws IOException
	{
		if(ValidationHelper.Validate(scriptKey))
		{
			List<Cat> cats = em.createNamedQuery("getCatById", Cat.class).setParameter("id", dbid).getResultList();
			Cat cat = null;
			if(cats.size() > 0)
				cat = cats.get(0);
			else
			{
				cat = (Cat)em.createNamedQuery("getCatByCatId").setParameter("catid", id).getResultList().get(0);
			}
			if(cat != null && cat.isSick())
			{
				cat.heal();
				em.persist(cat);
				response.setContentType("text/plain");
				response.setStatus(200);
				ServletOutputStream outStream = response.getOutputStream();
				outStream.print("ok");
				outStream.flush();
		    	outStream.close();
			}
			else
				response.sendError(404);
		}
		else
		{
			response.sendError(403);
		}
	}
	
	@RequestMapping("/traits")
	public void getTraitString(@RequestHeader(value="Script-Key", required=true) String scriptKey,  @RequestParam(value="dbid", required=true) long dbid,@RequestHeader("X-SecondLife-Object-Key") String id, @RequestParam(value="type", required=true) String type,HttpServletResponse response) throws IOException
	{
		if(ValidationHelper.Validate(scriptKey))
		{
			
			List<Cat> cats = em.createNamedQuery("getCatById", Cat.class).setParameter("id", dbid).getResultList();
			Cat cat = null;
			if(cats.size() > 0)
				cat = cats.get(0);
			else
			{
				if(type.equals("basket"))
				{
					cat = (Cat)em.createNamedQuery("getCatByBasketId").setParameter("basketid", id).getResultList().get(0);
				}
				else if(type.equals("cat"))
				{
					cat = (Cat)em.createNamedQuery("getCatByCatId").setParameter("catid", id).getResultList().get(0);
				}
			}
			if(cat != null)
			{
				response.setContentType("text/plain");
				response.setStatus(200);
				ServletOutputStream outStream = response.getOutputStream();
				String traits = cat.getTraitString();
				outStream.print(traits);
				outStream.flush();
		    	outStream.close();
			}
			else
			{
				response.sendError(404);
			}
		}
		else
		{
			response.sendError(403);
		}
	}
	
	@Autowired
	private CatRepository repository;
	
	@RequestMapping("/init")
	public void initCat(@RequestHeader(value="Script-Key", required=true) String scriptKey, @RequestParam(value="dbid", required=true) long dbid,@RequestHeader("X-SecondLife-Region") String region,@RequestHeader("X-SecondLife-Object-Key") String catid,@RequestHeader("X-SecondLife-Owner-Key") String ownerid, @RequestHeader(value = "Group-id", required=true) String group, HttpServletResponse response) throws IOException
	{
		if(ValidationHelper.Validate(scriptKey))
		{
			try
			{
				Cat cat = repository.findOne(dbid); 
				int wildCatnum = em.createNamedQuery("getCatTypeByName").setParameter("name", "Wild Cat").getResultList().size();
				List cats = em.createNamedQuery("getAllCats").getResultList();
				if(cat == null)
				{
					cat = (Cat)em.createNamedQuery("getCatByCatId").setParameter("catid", catid).getResultList().get(0);
				}
				Owner o = Owner.getOrCreateOwner(ownerid, em);
				cat.birthCheck();
				cat.setOwner(o);
				cat.setCatid(catid);
				cat.setGroupid(group);
				cat.setLandid(region);
				cat.setLastupdate(new Timestamp(System.currentTimeMillis()));
				em.persist(cat);
				DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
				DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
				Document doc = dBuilder.newDocument();
				
				Element catElement = doc.createElement("Cat");
				doc.appendChild(catElement);
				
				catElement.appendChild(cat.getProperties(doc));
				catElement.appendChild(cat.getParametersElement(doc, em));
				
				response.setContentType("application/xml");
				ServletOutputStream outStream = response.getOutputStream();
				
				Transformer transformer = TransformerFactory.newInstance().newTransformer();
		        transformer.setOutputProperty(OutputKeys.INDENT, "yes"); 
		        DOMSource source = new DOMSource(doc);
		        StreamResult body = new StreamResult(outStream);
		        
		        
		        response.setStatus(200);
		        transformer.transform(source, body);
		        
			    outStream.flush();
			    outStream.close();
			}
			catch(Exception ex)
			{
				ex.printStackTrace();
				response.sendError(404);
			}
			
		}
	}
	
	@RequestMapping("/Name")
	public void setName(@RequestParam(value="dbid", required=true) long dbid,@RequestHeader(value="Script-Key", required=true) String scriptKey,@RequestHeader("X-SecondLife-Region") String region,@RequestHeader("X-SecondLife-Object-Key") String catid,@RequestHeader("X-SecondLife-Owner-Key") String ownerid, @RequestHeader(value = "Group-id", required=true) String group,@RequestHeader("CatName") String name,HttpServletResponse response) throws IOException, ParserConfigurationException, TransformerFactoryConfigurationError, TransformerException
	{
		if(ValidationHelper.Validate(scriptKey))
		{
			Cat cat = repository.findOne(dbid); 
			if(cat == null)
			{
				cat = (Cat)em.createNamedQuery("getCatByCatId").setParameter("catid", catid).getResultList().get(0);
			}
			
			if(cat != null)
			{
				Owner o = Owner.getOrCreateOwner(ownerid, em);
				cat.birthCheck();
				cat.setOwner(o);
				cat.setCatid(catid);
				cat.setName(name);
				cat.setGroupid(group);
				cat.setLandid(region);
				cat.setLastupdate(new Timestamp(System.currentTimeMillis()));
				em.persist(cat);
				response.setStatus(200);
			}
			else
				response.sendError(404);
		}
		else
			response.sendError(403);
	}
	
	@RequestMapping("/basket")
	public void getBasketedCat(@RequestParam(value="id", required=true) String id, @RequestParam(value="dbid", required=true) long dbid,@RequestHeader(value="Script-Key", required=true) String scriptKey,@RequestHeader("X-SecondLife-Object-Key") String basketid,@RequestHeader("X-SecondLife-Owner-Key") String ownerid,HttpServletResponse response) throws IOException, ParserConfigurationException, TransformerFactoryConfigurationError, TransformerException
	{
		if(ValidationHelper.Validate(scriptKey))
		{
			List<Cat> cats = (List<Cat>)em.createNamedQuery("getCatById").setParameter("id", dbid).getResultList();
			Cat cat =null;
			if(cats.size() > 0)
				cat = cats.get(0);
			else
			{
				cat = catService.generateOrReturnBasket(Integer.parseInt(id), basketid, ownerid);
			}
			if(cat != null)
			{
				cat.setBasketid(basketid);
				Owner o = Owner.getOrCreateOwner(ownerid, em);
				cat.setOwner(o);
				em.persist(cat);
				DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
				DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
				Document doc = dBuilder.newDocument();
				
				Element catElement = doc.createElement("Cat");
				doc.appendChild(catElement);
				
				Element genderElement = doc.createElement("gender");
				genderElement.setTextContent(cat.isMale()? "male":"female");
				catElement.appendChild(genderElement);
				
				Element heightElement = doc.createElement("height");
				heightElement.setTextContent(Double.toString(cat.getHeight1()));
				catElement.appendChild(heightElement);
				
				Element hoverTextElement = doc.createElement("hovertext");
				hoverTextElement.setTextContent(cat.getHoverString());
				catElement.appendChild(hoverTextElement);
				
				Element dbidElement = doc.createElement("dbid");
				dbidElement.setTextContent(Long.toString(cat.getId()));
				catElement.appendChild(dbidElement);
				
				response.setContentType("application/xml");
				ServletOutputStream outStream = response.getOutputStream();
				response.setStatus(200);
				Transformer transformer = TransformerFactory.newInstance().newTransformer();
		        transformer.setOutputProperty(OutputKeys.INDENT, "yes"); 
		        DOMSource source = new DOMSource(doc);
		        StreamResult body = new StreamResult(outStream);
		        transformer.transform(source, body);
		        outStream.flush();
		        outStream.close();
			}
			else
				response.sendError(404);
		}
		else
			response.sendError(403);
		
	}
	
	@RequestMapping("/Settings")
	public void setSettings(@RequestHeader(value="Group-id", required=true) String group, @RequestParam(value="dbid", required=true) long dbid, @RequestParam(value="mate", required=false, defaultValue = "-1") long mate,@RequestHeader("X-SecondLife-Region") String region,@RequestParam(value="breeding", required=false) String breeding, @RequestParam(value="hovername", required=false) String hovername, @RequestParam(value="hoverage", required=false) String hoverage, @RequestParam(value="hoverbreeding", required=false) String hoverbreeding, @RequestParam(value="hoverneeds", required=false) String hoverneeds,@RequestParam(value="birth", required=false) String birth,@RequestHeader(value="Script-Key", required=true) String scriptKey,@RequestHeader("X-SecondLife-Object-Key") String catid,@RequestHeader("X-SecondLife-Owner-Key") String ownerid ,HttpServletResponse response) throws IOException
	{
		if(ValidationHelper.Validate(scriptKey))
		{
			try
			{
				List<Cat> cats = em.createNamedQuery("getCatById", Cat.class).setParameter("id", dbid).getResultList();
				Cat cat =null;
				if(cats.size() > 0)
					cat = cats.get(0);
				else
				{
					cat = (Cat)em.createNamedQuery("getCatByCatId").setParameter("catid", catid).getResultList().get(0);
				}
				if(cat != null)
				{
					Owner o = Owner.getOrCreateOwner(ownerid, em);
					cat.setOwner(o);
					cat.setGroupid(group);
					cat.setLandid(region);
					cat.setCatid(catid);
					em.persist(cat);
					if(breeding != null && !breeding.isEmpty())
					{
						if(breeding.equals("owner"))
							cat.getCatsettings().setBreedingsettings(BreedingSettings.OwnerOnly);
						else if (breeding.equals("everyone"))
							cat.getCatsettings().setBreedingsettings(BreedingSettings.Everyone);
						else if (breeding.equals("group"))
							cat.getCatsettings().setBreedingsettings(BreedingSettings.GroupOnly);
						else if (breeding.equals("mate"))
						{
							if(mate != -1)
							{
								Cat m =(Cat)em.createNamedQuery("getCatById").setParameter("id", mate).getResultList().get(0);
								if(m != null)
								{
									cat.getCatsettings().setMate(m);
									cat.getCatsettings().setBreedingsettings(BreedingSettings.Mate);
								}
							}
						}
					}
					if(birth != null && !birth.isEmpty())
					{
						if(birth.equals("on"))
							cat.getCatsettings().setBirthsettings(true);
						else if(birth.equals("off"))
							cat.getCatsettings().setBirthsettings(false);
					}
					if(hovername != null && !hovername.isEmpty())
					{
						if(hovername.equals("on"))
							cat.getCatsettings().setHovername(true);
						else if(hovername.equals("off"))
							cat.getCatsettings().setHovername(false);
					}
					if(hoverage != null && !hoverage.isEmpty())
					{
						if(hoverage.equals("on"))
							cat.getCatsettings().setHoverage(true);
						else if(hoverage.equals("off"))
							cat.getCatsettings().setHoverage(false);
					}
					if(hoverbreeding != null && !hoverbreeding.isEmpty())
					{
						if(hoverbreeding.equals("on"))
							cat.getCatsettings().setHoverbreeding(true);
						else if(hoverbreeding.equals("off"))
							cat.getCatsettings().setHoverbreeding(false);
					}
					if(hoverneeds != null && !hoverneeds.isEmpty())
					{
						if(hoverneeds.equals("on"))
							cat.getCatsettings().setHoverneeds(true);
						else if(hoverneeds.equals("off"))
							cat.getCatsettings().setHoverneeds(false);
					}
					em.persist(cat);
					response.setStatus(200);
					em.flush();
				}
			} catch(Exception ex)
			{
				ex.printStackTrace();
				response.sendError(404);
			}
		}
		else
			response.sendError(403);
	}
	

}