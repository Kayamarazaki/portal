package com.feistyfelinesbreedables.portal.controller;
import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.feistyfelinesbreedables.portal.service.CreationService;
@Controller
public class HomeController 
{
	@Autowired
	EntityManager em;
@RequestMapping("/test")
	public String hello() 
	{
		return "helloworld";
	}
}