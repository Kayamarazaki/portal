package com.feistyfelinesbreedables.portal.controller;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.EntityManager;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.feistyfelinesbreedables.portal.entity.Cat;
import com.feistyfelinesbreedables.portal.repository.CatRepository;

@Controller
@RequestMapping("/Breeding")
@Transactional
public class BreedingController {
	@Autowired
	private EntityManager em;
	
	@Autowired
	private CatRepository repository;
	/*@RequestMapping("/master")
	public void masterBreed(@RequestParam(value="father", required=true) long father, @RequestParam(value="mother", required=true) long mother,HttpServletResponse response) throws IOException
	{
		Cat f = repository.findOne(father);
		Cat m = repository.findOne(mother);
		
		if(f!=null && m != null)
		{
			Cat cat = new Cat(f,m,f.getOwner(),em);
			if(cat != null)
			{
				em.persist(cat);
				response.setContentType("text/plain");
				response.setStatus(200);
				ServletOutputStream outStream = response.getOutputStream();
				outStream.print(Long.toString(cat.getId()));
				outStream.flush();
		    	outStream.close();
			}
		}
	}*/
	
	@RequestMapping("/try")
	public void tryBreed(@RequestHeader("Script-Key") String scriptKey, @RequestParam(value="dbid", required=true) long dbid,@RequestHeader("X-SecondLife-Object-Key") String maleid, @RequestParam(value="id", required=true) String femaleid,HttpServletResponse response) throws IOException
	{
		List<Cat> lim = em.createNamedQuery("getCatById", Cat.class).setParameter("id", dbid).getResultList();
		if(lim.size() < 1)
		{
			lim = em.createNamedQuery("getCatByCatId", Cat.class).setParameter("catid", maleid).getResultList();
		}
		List<Cat> lif = em.createNamedQuery("getCatByCatId", Cat.class).setParameter("catid", femaleid).getResultList();
		
		if(lim.size() == 0 || lif.size() == 0)
		{
			response.setStatus(401);
		}
		else
		{
			Cat male = lim.get(0);
			Cat female = lif.get(0);
			
			
			if(male.getHeat() == 100.0 && female.getPregnantfrom() == null && female.getHeat() == 100.0 && female.isMale() == false && female.getCyclesleft() > 0)
			{
				if(male != null && Cat.canBreed(male, female))
				female.setPregnantfrom(male, em);
			}
			
		}
		
		
	}
}
