package com.feistyfelinesbreedables.portal.controller;

import java.io.IOException;
import java.util.List;

import javax.persistence.EntityManager;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.feistyfelinesbreedables.portal.entity.Cat;
import com.feistyfelinesbreedables.portal.entity.Owner;
import com.feistyfelinesbreedables.portal.helpers.ValidationHelper;

@Controller
@RequestMapping("/Owner")
@Transactional
public class OwnerController {
	@Autowired
	private EntityManager em;
	
	@Autowired
	PasswordEncoder encoder;
	
	@RequestMapping("/getRegisterKey")
	public void getLink(@RequestHeader("Script-Key") String scriptKey, @RequestHeader("X-SecondLife-Owner-Key") String ownerid, HttpServletResponse response) throws IOException
	{
		if(ValidationHelper.Validate(scriptKey))
		{
			response.setContentType("text/plain");
			String sleutel = encoder.encode(ownerid);
			
			response.setStatus(200);
			ServletOutputStream outStream = response.getOutputStream();
			String link = "You can register your account through the following secure link:\n http://www.feistyfelinesbreedables.com/register?avatar="+ownerid+"&key="+sleutel;
			outStream.print(link);
			outStream.flush();
	    	outStream.close();
		}
		else
		{
			response.sendError(403);
		}
	}
	
	@RequestMapping(path="/getPawPoints")
	public void getPawPoints(@RequestHeader("Script-Key") String scriptKey, @RequestHeader("X-SecondLife-Owner-Key") String ownerid, HttpServletResponse response) throws IOException
	{
		if(ValidationHelper.Validate(scriptKey))
		{
			Owner o = Owner.getOrCreateOwner(ownerid, em);
			String returns = "You currently have " + Integer.toString(o.getPawpoints()) + " Paw Points.";
			
			response.setContentType("text/plain");
			response.setStatus(200);
			ServletOutputStream outStream = response.getOutputStream();
			outStream.print(returns);
			outStream.flush();
	    	outStream.close();
		}
		else
		{
			response.sendError(403);
		}	
	}
	
	@RequestMapping(path="/catsinregion")
	public void catsInRegion(@RequestHeader("Script-Key") String scriptKey, @RequestHeader("X-SecondLife-Region") String region,@RequestHeader("X-SecondLife-Owner-Key") String ownerid, HttpServletResponse response) throws IOException
	{
		if(ValidationHelper.Validate(scriptKey))
		{
			Owner o = Owner.getOrCreateOwner(ownerid, em);
			List<Cat> list = em.createNamedQuery("getCatsByOwnerAndRegion",Cat.class).setParameter("owner", o).setParameter("region", region).getResultList();
			int i = list.size();
			String returns = "";
			while(--i >= 0)
			{
				returns += list.get(i).getCatid();
				if(i != 0)
					returns += ",";
			}
			if(returns != "")
			{
				response.setContentType("text/plain");
				response.setStatus(200);
				ServletOutputStream outStream = response.getOutputStream();
				outStream.print(returns);
				outStream.flush();
		    	outStream.close();
			}
			else
			{
				response.sendError(404);
			}
			
		}
		else
		{
			response.sendError(403);
		}	
	}
	
	@RequestMapping(path="/vendorfood")
	public void vendorFood(@RequestParam(value="num", required=true) int num,@RequestHeader("Script-Key") String scriptKey, @RequestParam(value="id", required=true) String ownerid, HttpServletResponse response) throws IOException
	{
		if(ValidationHelper.Validate(scriptKey))
		{
			Owner o = Owner.getOrCreateOwner(ownerid, em);
			
			o.setFood(o.getFood() + 168 * num);
			em.persist(o);
			
			response.setStatus(200);
		}
		else
		{
			response.sendError(403);
		}
	}
	
	@RequestMapping(path="/vendorhappiness")
	public void vendorHappiness(@RequestParam(value="num", required=true) int num,@RequestHeader("Script-Key") String scriptKey, @RequestParam(value="id", required=true) String ownerid, HttpServletResponse response) throws IOException
	{
		if(ValidationHelper.Validate(scriptKey))
		{
			Owner o = Owner.getOrCreateOwner(ownerid, em);
			
			o.setHappiness(o.getHappiness() + 168 * num);
			em.persist(o);
			
			response.setStatus(200);
		}
		else
		{
			response.sendError(403);
		}
	}
	
	@RequestMapping(path="/food", method = RequestMethod.POST)
	public void addFood(@RequestParam(value="num", required=true) int num,@RequestHeader("Script-Key") String scriptKey,@RequestHeader("X-SecondLife-Owner-Key") String ownerid,HttpServletResponse response) throws IOException
	{
		if(ValidationHelper.Validate(scriptKey))
		{
			Owner o = Owner.getOrCreateOwner(ownerid, em);
			
			o.setFood(o.getFood() + num);
			em.persist(o);
			
			response.setStatus(200);
		}
		else
		{
			response.sendError(403);
		}
	}
	@RequestMapping(path="/health", method = RequestMethod.POST)
	public void addHealth(@RequestParam(value="num", required=true) int num,@RequestHeader("Script-Key") String scriptKey,@RequestHeader("X-SecondLife-Owner-Key") String ownerid,HttpServletResponse response) throws IOException
	{
		if(ValidationHelper.Validate(scriptKey))
		{
			Owner o = Owner.getOrCreateOwner(ownerid, em);
			
			o.setHealthpack(o.getHealthpack() + num);
			em.persist(o);
			
			response.setStatus(200);
		}
		else
		{
			response.sendError(403);
		}
	}
	@RequestMapping(path="/happiness", method = RequestMethod.POST)
	public void addHappiness(@RequestParam(value="num", required=true) int num,@RequestHeader("Script-Key") String scriptKey,@RequestHeader("X-SecondLife-Owner-Key") String ownerid,HttpServletResponse response) throws IOException
	{
		if(ValidationHelper.Validate(scriptKey))
		{
			Owner o = Owner.getOrCreateOwner(ownerid, em);
			
			o.setHappiness(o.getHappiness() + num);
			em.persist(o);
			
			response.setStatus(200);
		}
		else
		{
			response.sendError(403);
		}
	}
	
	@RequestMapping(path="/happiness", method = RequestMethod.GET)
	public void getHappiness(@RequestHeader("Script-Key") String scriptKey,@RequestHeader("X-SecondLife-Owner-Key") String ownerid,HttpServletResponse response) throws IOException
	{
		if(ValidationHelper.Validate(scriptKey))
		{
			Owner o = Owner.getOrCreateOwner(ownerid, em);
			int num = o.getHappiness();
			response.setContentType("text/plain");
			response.setStatus(200);
			ServletOutputStream outStream = response.getOutputStream();
	
			outStream.print(num);
			outStream.flush();
			outStream.close();
		}
		else
		{
			response.sendError(403);
		}
	}

	@RequestMapping(path="/food", method = RequestMethod.GET)
	public void getFood(@RequestHeader("Script-Key") String scriptKey,@RequestHeader("X-SecondLife-Owner-Key") String ownerid,HttpServletResponse response) throws IOException
	{
		if(ValidationHelper.Validate(scriptKey))
		{
			Owner o = Owner.getOrCreateOwner(ownerid, em);
			int num = o.getFood();
			response.setContentType("text/plain");
			response.setStatus(200);
			ServletOutputStream outStream = response.getOutputStream();
	
			outStream.print(num);
			outStream.flush();
			outStream.close();
		}
		else
		{
			response.sendError(403);
		}
	}
	
	@RequestMapping(path="/health", method = RequestMethod.GET)
	public void getHealth(@RequestHeader("Script-Key") String scriptKey,@RequestHeader("X-SecondLife-Owner-Key") String ownerid,HttpServletResponse response) throws IOException
	{
		if(ValidationHelper.Validate(scriptKey))
		{
			Owner o = Owner.getOrCreateOwner(ownerid, em);
			int num = o.getHealthpack();
			response.setContentType("text/plain");
			response.setStatus(200);
			ServletOutputStream outStream = response.getOutputStream();
	
			outStream.print(num);
			outStream.flush();
			outStream.close();
		}
		else
		{
			response.sendError(403);
		}
	}
}
