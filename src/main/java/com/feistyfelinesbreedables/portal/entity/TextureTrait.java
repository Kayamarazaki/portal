package com.feistyfelinesbreedables.portal.entity;

import java.util.Hashtable;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
@MappedSuperclass
public class TextureTrait extends Trait {
	public TextureTrait()
	{
		
	}
	
	public TextureTrait(String name, String texturemaps, String texturetypes)
	{
		super(name);
		this.texturetypes = texturetypes;
		this.texturemaps = texturemaps;
	}
	public String getTexturetypes() {
		return texturetypes;
	}

	public void setTexturetypes(String texturetypes) {
		this.texturetypes = texturetypes;
	}

	public String getTexturemaps() {
		return texturemaps;
	}

	public void setTexturemaps(String texturemaps) {
		this.texturemaps = texturemaps;
	}
	@Column(length=1000)
	protected String texturetypes;
	@Column(length=1000)
	protected String texturemaps;
	
	public Hashtable<String,String> getParameters() throws Exception
	{
		Hashtable<String,String> table = new Hashtable();
		String[] types = texturetypes.replace("[", "").replace("]", "").split("/");
		String[] textures = texturemaps.replace("[", "").replace("]", "").split("/");
		
		if(types.length == textures.length)
		{
			for(int i = 0;i<types.length;i++)
			{
				table.put(types[i],textures[i]);
			}
			return table;
		}
		else
		{
			throw new Exception("Wrong length of arrays");
		}
	}
}
