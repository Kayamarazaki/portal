package com.feistyfelinesbreedables.portal.entity;

import java.util.Hashtable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@NamedQueries({
@NamedQuery(name="getCatTypeByName", query="SELECT ct FROM CatType ct WHERE ct.name = :name"),
@NamedQuery(name="getCatTypeById", query="SELECT ct FROM CatType ct WHERE ct.id= :id")
})
@Table(name = "cattype")
public class CatType  {
	public CatType(String name, String sculptmaps, String sculpttypes)
	{
		this.name = name;
		this.sculptmaps = sculptmaps; 
		this.sculpttypes =sculpttypes;
	}
	
	protected CatType()
	{
		
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	
	private int occurence;
	
	public int getOccurence() {
		return occurence;
	}

	public void setOccurence(int occurence) {
		this.occurence = occurence;
	}

	public String getSculpttypes() {
		return sculpttypes;
	}

	public void setSculpttypes(String sculpttypes) {
		this.sculpttypes = sculpttypes;
	}

	public String getSculptmaps() {
		return sculptmaps;
	}

	public void setSculptmaps(String sculptmaps) {
		this.sculptmaps = sculptmaps;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getPosition_gen_1() {
		return position_gen_1;
	}

	public void setPosition_gen_1(String position_gen_1) {
		this.position_gen_1 = position_gen_1;
	}

	public String getPosition_gen_2() {
		return position_gen_2;
	}

	public void setPosition_gen_2(String position_gen_2) {
		this.position_gen_2 = position_gen_2;
	}

	public String getPosition_gen_3() {
		return position_gen_3;
	}

	public void setPosition_gen_3(String position_gen_3) {
		this.position_gen_3 = position_gen_3;
	}

	public String getSize_gen_1() {
		return size_gen_1;
	}

	public void setSize_gen_1(String size_gen_1) {
		this.size_gen_1 = size_gen_1;
	}

	public String getSize_gen_2() {
		return size_gen_2;
	}

	public void setSize_gen_2(String size_gen_2) {
		this.size_gen_2 = size_gen_2;
	}

	public String getSize_gen_3() {
		return size_gen_3;
	}

	public void setSize_gen_3(String size_gen_3) {
		this.size_gen_3 = size_gen_3;
	}

	public String getRotation() {
		return rotation;
	}

	public void setRotation(String rotation) {
		this.rotation = rotation;
	}

	public String getMirror() {
		return mirror;
	}

	public void setMirror(String mirror) {
		this.mirror = mirror;
	}

	public String getStitching() {
		return stitching;
	}

	public void setStitching(String stitching) {
		this.stitching = stitching;
	}

	public List<Pelt> getPelts() {
		return pelts;
	}

	public void setPelts(List<Pelt> pelts) {
		this.pelts = pelts;
	}
	
	private double height1;
	private double height2;
	private double height3;
	
	public double getHeight1() {
		return height1;
	}

	public void setHeight1(double height1) {
		this.height1 = height1;
	}

	public double getHeight2() {
		return height2;
	}

	public void setHeight2(double height2) {
		this.height2 = height2;
	}

	public double getHeight3() {
		return height3;
	}

	public void setHeight3(double height3) {
		this.height3 = height3;
	}

	@Column(length=1000)
	private String position_gen_1;
	@Column(length=1000)
	private String position_gen_2;
	@Column(length=1000)
	private String position_gen_3;
	@Column(length=1000)
	private String size_gen_1;
	@Column(length=1000)
	private String size_gen_2;
	@Column(length=1000)
	private String size_gen_3;
	@Column(length=1000)
	private String rotation;
	@Column(length=1000)
	private String mirror;
	@Column(length=1000)
	private String stitching;
	@Column(length=1000)
	private String sculpttypes;
	@Column(length=1000)
	private String sculptmaps;
	
	private String name;
	
	@OneToMany(mappedBy="cattype")
	private List<Pelt> pelts;
	
	public Hashtable<String, Hashtable<String,String>> getParameters(int age) throws Exception
	{
		Hashtable<String, Hashtable<String,String>> table = new Hashtable();
		
		String[] types = sculpttypes.replace("[", "").replace("]", "").split("/");
		String[] maps = sculptmaps.replace("[", "").replace("]", "").split("/");
		String[] pos1 = position_gen_1.replace("[", "").replace("]", "").split("/");
		String[] pos2 = position_gen_2.replace("[", "").replace("]", "").split("/");
		String[] pos3 = position_gen_3.replace("[", "").replace("]", "").split("/");
		String[] size1 = size_gen_1.replace("[", "").replace("]", "").split("/");
		String[] size2 = size_gen_2.replace("[", "").replace("]", "").split("/");
		String[] size3 = size_gen_3.replace("[", "").replace("]", "").split("/");
		String[] rots = rotation.replace("[", "").replace("]", "").split("/");
		String[] mirr = mirror.split("/");
		String[] stitch = stitching.split("/");

		if(types.length == maps.length && types.length == pos1.length&& types.length == pos2.length && types.length == pos3.length && types.length == size1.length && types.length == size2.length && types.length == size3.length && types.length == rots.length && types.length == mirr.length && types.length == stitch.length)
		{
			for(int i = 0;i<types.length;i++)
			{
				Hashtable<String,String> t = new Hashtable();
				
				t.put("sculptmap", maps[i]);
				if(age < 3)
					t.put("pos", pos1[i]);
				else if (age < 5)
					t.put("pos", pos2[i]);
				else
					t.put("pos", pos3[i]);
				if(age < 3)
					t.put("size", size1[i]);
				else if (age < 5)
					t.put("size", size2[i]);
				else
					t.put("size", size3[i]);
				t.put("rot", rots[i]);
				t.put("mirror", mirr[i]);
				t.put("stitching", stitch[i]);
				t.put("height1", Double.toString(height1));
				t.put("height2", Double.toString(height3));
				t.put("height3", Double.toString(height3));
				
				table.put(types[i], t);
			}
			
		}
		else
		{
			throw new Exception("Wrong length of arrays");
		}
		
		return table;
	}
}
