package com.feistyfelinesbreedables.portal.entity;

public enum TraitType {
	Starter(0), Breeding(1), LimitedNoPass(2), LimitedPass(3);
	
	private final int value;
    private TraitType(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
