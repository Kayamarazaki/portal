package com.feistyfelinesbreedables.portal.entity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.feistyfelinesbreedables.portal.BreedingSettings;
@Entity
@Table(name="catsettings")
public class CatSettings {
	public CatSettings()
	{
		breedingsettings = BreedingSettings.OwnerOnly;
		birthsettings = true;
		hovername = true;
		hoverage = true;
		hoverneeds = true;
		hoverbreeding = true;
	}
	@OneToOne(optional=false, mappedBy="catsettings")
    private Cat cat;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	
	
	private boolean hovername;
	private boolean hoverage;
	private boolean hoverneeds;
	private boolean hoverbreeding;
	
	public boolean getHovername() {
		return hovername;
	}

	public void setHovername(boolean hovername) {
		this.hovername = hovername;
	}

	public boolean getHoverage() {
		return hoverage;
	}

	public void setHoverage(boolean hoverage) {
		this.hoverage = hoverage;
	}

	public boolean getHoverneeds() {
		return hoverneeds;
	}

	public void setHoverneeds(boolean hoverneeds) {
		this.hoverneeds = hoverneeds;
	}

	public boolean getHoverbreeding() {
		return hoverbreeding;
	}

	public void setHoverbreeding(boolean hoverbreeding) {
		this.hoverbreeding = hoverbreeding;
	}
	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Cat mate;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	
	
	public String getBreedingString()
	{
		if(breedingsettings == BreedingSettings.OwnerOnly)
			return "Owner";
		if(breedingsettings == BreedingSettings.GroupOnly)
			return "Group";
		if(breedingsettings == BreedingSettings.Everyone)
			return "All";
		if(mate != null)
			return mate.getCatid();
		return "";
	}
	
	public boolean getBirthsettings() {
		return birthsettings;
	}

	public void setBirthsettings(boolean birthsettings) {
		this.birthsettings = birthsettings;
	}


	protected boolean birthsettings;
	
	@Enumerated(EnumType.ORDINAL)
	protected BreedingSettings breedingsettings;
	public BreedingSettings getBreedingsettings() {
		return breedingsettings;
	}


	public Cat getCat() {
		return cat;
	}

	public void setCat(Cat cat) {
		this.cat = cat;
	}

	public Cat getMate() {
		return mate;
	}

	public void setMate(Cat mate) {
		this.mate = mate;
	}

	public void setDefault() {
		this.setBreedingsettings(BreedingSettings.OwnerOnly);
		this.setBirthsettings(true);
	}
	
	public void setBreedingsettings(BreedingSettings settings){
		this.breedingsettings = settings;
		if(settings != BreedingSettings.Mate)
			this.mate=null;
	}
	
}
