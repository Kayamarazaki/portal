package com.feistyfelinesbreedables.portal.entity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@NamedQueries({
	@NamedQuery(name="getAllTails", query = "SELECT t FROM Tail t"),
@NamedQuery(name="getTailByName",query="SELECT t FROM Tail t WHERE t.name = :name"),
@NamedQuery(name="getTailStarter",query="SELECT t FROM Tail t WHERE t.traittype = com.feistyfelinesbreedables.portal.entity.TraitType.Starter"),
@NamedQuery(name="getTailBreeding",query="SELECT t FROM Tail t WHERE t.traittype = com.feistyfelinesbreedables.portal.entity.TraitType.Breeding"),
@NamedQuery(name="getTailById", query="SELECT t FROM Tail t WHERE t.id= :id")
})
@Table(name="tail")
public class Tail extends SculptTrait{
	public Tail(String name, String sculpttypes, String sculptmaps)
	{
		super(name, sculpttypes,sculptmaps);
	}
	
	protected Tail()
	{
		
	}
	
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;

	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public CatType getCattype() {
		return cattype;
	}

	public void setCattype(CatType cattype) {
		this.cattype = cattype;
	}


	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private CatType cattype;
}
