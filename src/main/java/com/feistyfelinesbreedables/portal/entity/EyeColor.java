package com.feistyfelinesbreedables.portal.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@NamedQueries({
	@NamedQuery(name="getAllEyeColors", query = "SELECT e FROM EyeColor e"),
@NamedQuery(name="getEyeColorByName",query="SELECT e FROM EyeColor e WHERE e.name = :name"),
@NamedQuery(name="getEyeColorStarter",query="SELECT e FROM EyeColor e WHERE e.traittype = com.feistyfelinesbreedables.portal.entity.TraitType.Starter"),
@NamedQuery(name="getEyeColorBreeding",query="SELECT e FROM EyeColor e WHERE e.traittype = com.feistyfelinesbreedables.portal.entity.TraitType.Breeding"),
@NamedQuery(name="getEyeColorById", query="SELECT ec FROM EyeColor ec WHERE ec.id= :id")
})
@Table(name="eyecolor")
public class EyeColor extends Trait {
	
	public EyeColor(String name)
	{
		super(name);
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	protected EyeColor()
	{
	
	}
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
}
