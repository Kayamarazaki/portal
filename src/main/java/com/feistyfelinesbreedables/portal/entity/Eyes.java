package com.feistyfelinesbreedables.portal.entity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name="eyes")
@NamedQuery(name="getEyesByName", query="SELECT e FROM Eyes e WHERE e.name = :name")
public class Eyes extends TextureTrait{
	public Eyes(EyeColor c, EyeShape eyeshape, PupilShape pupilshape, String texturemaps, String texturetypes)
	{
		super(getQualifiedName(c,eyeshape,pupilshape),texturemaps,texturetypes);
		
		eyecolor = c;
		this.eyeshape = eyeshape;
		this.pupilshape = pupilshape;
	}
	
	protected Eyes()
	{
		
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	
	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private EyeColor eyecolor;
	
	public EyeColor getEyecolor() {
		return eyecolor;
	}

	public void setEyecolor(EyeColor eyecolor) {
		this.eyecolor = eyecolor;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public PupilShape getPupilshape() {
		return pupilshape;
	}

	public void setPupilshape(PupilShape pupilshape) {
		this.pupilshape = pupilshape;
	}

	public EyeShape getEyeshape() {
		return eyeshape;
	}

	public void setEyeshape(EyeShape eyeshape) {
		this.eyeshape = eyeshape;
	}
	
	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private PupilShape pupilshape;
	
	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private EyeShape eyeshape;
	
	public static String getQualifiedName(EyeColor c, EyeShape eyeshape, PupilShape pupilshape)
	{
		return c.getName()+eyeshape.getName()+pupilshape.getName();
	}
}
