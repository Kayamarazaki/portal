package com.feistyfelinesbreedables.portal.entity;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import com.feistyfelinesbreedables.portal.BreedingSettings;
import com.feistyfelinesbreedables.portal.engine.BreedingEngine;
import com.feistyfelinesbreedables.portal.engine.CatGenerationType;
import com.feistyfelinesbreedables.portal.scheduler.Scheduler;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.Hibernate;
import org.hibernate.annotations.Proxy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Query;

@Entity
@NamedQueries({
@NamedQuery(name="getCatsByOwnerAndRegion",query = "SELECT c FROM Cat c WHERE c.owner = :owner AND c.landid = :region"),
@NamedQuery(name="getCatAndParentsById",query = "SELECT c, f, m FROM Cat c JOIN c.father f JOIN c.mother m WHERE c.id = :id"),
@NamedQuery(name="getAllCats",query = "SELECT c FROM Cat c"),
@NamedQuery(name="getAllOwnedCats",query = "SELECT c FROM Cat c WHERE c.owner IS NOT NULL"),
@NamedQuery(name="getCatByBasketId", query="SELECT c FROM Cat c WHERE c.basketid = :basketid"),
@NamedQuery(name="getCatByCatId", query="SELECT c FROM Cat c WHERE c.catid = :catid"),
@NamedQuery(name="getCatById", query="SELECT c FROM Cat c WHERE c.id = :id"),
@NamedQuery(name="getCatPregnant", query="SELECT c FROM Cat c WHERE c.pregnantfrom IS NOT NULL AND c.lastupdate >= :lastupdate AND c.lastpregnancychange < :lastpregnancychange"),
@NamedQuery(name="getCatInactive", query="SELECT c FROM Cat c WHERE c.birth IS NOT NULL AND c.lastupdate < :lastupdate"),
@NamedQuery(name="getCatHUHA", query="SELECT c FROM Cat c WHERE c.birth IS NOT NULL AND c.lastupdate >= :lastupdate AND c.ispet = FALSE")
})
@Table(name="cat")
public class Cat {
	public Cat(Cat father, Cat mother, Owner owner, EntityManager em)
	{
		this(owner);
		this.father = father;
		father.setCyclesleft(father.getCyclesleft() - 1);
		this.mother = mother;
		mother.setCyclesleft(mother.getCyclesleft() - 1);
		this.readyforbirth = false;
		BreedingEngine.getInstance().generateOffspring(this,em);
		
	}

	public Cat(CatGenerationType type, String basketid, Owner owner, EntityManager em)
	{
		this(owner);
		BreedingEngine.getInstance().generateStarter(this,type, em);
		this.basketid = basketid;
	}
	
	protected Cat(Owner owner)
	{
		this();
		setOwner(owner);
	}

	
	public Cat()
	{
		hunger = 20;
		happiness = 80;		
		ispet = false;
	}
	
	public CatSettings getCatsettings() {
		return catsettings;
	}

	private int cyclesleft;
	
	public int getCyclesleft() {
		return cyclesleft;
	}

	public void setCyclesleft(int cyclesleft) {
		this.cyclesleft = cyclesleft;
	}

	public void setCatsettings(CatSettings catsettings) {
		this.catsettings = catsettings;
		catsettings.setCat(this);
	}

	@Id
	@GeneratedValue( strategy = GenerationType.SEQUENCE, generator = "CatSequenceGenerator" )
	private long id;
	
	private String name;
	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Whiskers whiskers;
	
	@OneToOne(optional = true, cascade = CascadeType.ALL)
	@JoinColumn(name="catsettings_id", unique=true, nullable=true, updatable=false)
	private CatSettings catsettings;
	
	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private Owner owner;
	
	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Cat pregnantfrom;
	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Cat pregnantwith;
	
	private boolean ispet;
	
	public boolean isIspet() {
		return ispet;
	}

	public void setIspet(boolean ispet) {
		this.ispet = ispet;
	}

	private boolean readyforbirth;
	
	public boolean isReadyforbirth() {
		return readyforbirth;
	}

	public void setReadyforbirth(boolean readyforbirth) {
		this.readyforbirth = readyforbirth;
	}

	@OneToMany(mappedBy = "mother")
	private List<Cat> motherBabies;
	
	public List<Cat> getBabiesReadyForBirth()
	{
		List<Cat> babies = new ArrayList<Cat>();
		if(motherBabies != null)
		{
			for(Cat cat : motherBabies)
			{
				if(cat.basketid == null && cat.readyforbirth)
				{
					babies.add(cat);
				}
			}
		}
		return babies;
	}
	
	public Cat getPregnantfrom() {
		return pregnantfrom;
	}

	public void setPregnantfrom(Cat pregnantFrom, EntityManager em) {
		if(this.pregnantfrom == null && !this.male)
		{
			Timestamp now =new Timestamp(System.currentTimeMillis());
			this.pregnantfrom = pregnantFrom;
			this.setLastpregnancychange(now);
			this.setHeat(0);
			
			this.setLastheatchange(now);
			pregnantFrom.setHeat(0);
			pregnantFrom.setLastheatchange(now);
			this.setPregnantwith(new Cat(pregnantFrom, this, this.owner, em));
		}
	}

	public Cat getPregnantwith() {
		return pregnantwith;
	}

	public void setPregnantwith(Cat pregnantWith) {
		this.pregnantwith = pregnantWith;
	}

	private boolean sick;
	public boolean isSick() {
		return sick;
	}

	public void setSick(boolean sick) {
		this.sick = sick;
	}
	
	public void heal()
	{
		int packs = this.owner.getHealthpack() ;
		if(this.sick && packs> 0)
		{
			this.owner.setHealthpack(packs - 1);
			this.sick = false;
			lastheal = new Timestamp(System.currentTimeMillis());
		}
	}
	
	public boolean recentlyhealed()
	{
		if(lastheal != null && System.currentTimeMillis() - lastheal.getTime() < 3600000)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	private String landid;
	public String getLandid() {
		return landid;
	}

	public void setLandid(String landid) {
		this.landid = landid;
	}

	public String getGroupid() {
		return groupid;
	}

	public void setGroupid(String groupid) {
		this.groupid = groupid;
	}

	private String groupid;
	
	private Timestamp lastheal;
	private Timestamp lastupdate;
	private Timestamp lastheatchange;
	private Timestamp lasthungerchange;
	private Timestamp lasthappinesschange;
	private Timestamp lastpregnancychange;
	
	public Timestamp getLastpregnancychange() {
		return lastpregnancychange;
	}

	public void setLastpregnancychange(Timestamp lastpregnancychange) {
		this.lastpregnancychange = lastpregnancychange;
	}

	private Timestamp lastbaby;
	
	
	public Timestamp getLastupdate() {
		return lastupdate;
	}

	public void setLastupdate(Timestamp lastupdate) {
		this.lastupdate = lastupdate;
	}

	public Owner getOwner() {
		return owner;
	}

	public void setOwner(Owner owner) {
		if(this.owner == null || !this.owner.getId().equals(owner.getId()))
		{
			this.owner = owner;
			if(catsettings == null)
				setCatsettings(new CatSettings());
			
			catsettings.setDefault();
			catsettings.setCat(this);
		}
	}

	private boolean male;
	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Cat father;
	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Cat mother;
	
	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private CatType cattype;
	
	
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isMale() {
		return male;
	}

	public void setMale(boolean male) {
		this.male = male;
		
		if(male)
		{
			cyclesleft = 18;
		}
		else
		{
			cyclesleft = 12;
		}
	}

	public CatType getCattype() {
		return cattype;
	}

	public void setCattype(CatType cattype) {
		this.cattype = cattype;
		
		this.setName("Feisty Felines "+ cattype.getName());
	}

	private Timestamp birth;
	
	private double hunger;
	
	private double heat;
	
	private double pregnancy;


	public Timestamp getLastheatchange() {
		return lastheatchange;
	}

	public void setLastheatchange(Timestamp lastheatchange) {
		this.lastheatchange = lastheatchange;
	}

	public Timestamp getLasthungerchange() {
		return lasthungerchange;
	}

	public void setLasthungerchange(Timestamp lasthungerchange) {
		this.lasthungerchange = lasthungerchange;
	}

	public Timestamp getLasthappinesschange() {
		return lasthappinesschange;
	}

	public void setLasthappinesschange(Timestamp lasthappinesschange) {
		this.lasthappinesschange = lasthappinesschange;
	}

	public Timestamp getLastbaby() {
		return lastbaby;
	}

	public void setLastbaby(Timestamp lastbaby) {
		this.lastbaby = lastbaby;
	}

	public double getHunger() {
		return hunger;
	}

	public void setHunger(double hunger) {
		if(hunger > 99.9)
		{
			this.hunger = 100;
			this.sick = true;
		}
		else if( hunger < 0.1)
		{
			this.hunger = 0;
		}
		else
		{
			this.hunger = hunger;
		}
	}

	public double getPregnancy() {
		return pregnancy;
	}

	public void setPregnancy(double pregnancy) {
		this.pregnancy = pregnancy;
		if(pregnancy > 99.9)
		{
			this.pregnancy= 0;
			this.pregnantfrom = null;
			this.pregnantwith = null;
		}
		else
			this.pregnancy= pregnancy;
		
	}


	public Timestamp getBirth() {
		return birth;
	}

	public void setBirth(Timestamp birth) {
		this.birth = birth;
		this.lasthappinesschange = birth;
		this.lastheatchange = birth;
		this.lasthungerchange = birth;
		
	}

	public double getHeat() {
		return heat;
	}

	public void setHeat(double heat) {
		if(heat > 99.9)
			this.heat = 100;
		else if (heat < 0.1)
			this.heat = 0;
		else
			this.heat = heat;
	}

	public double getHappiness() {
		return happiness;
	}

	public void setHappiness(double happiness) {
		if(happiness < 0.1)
			this.happiness = 0;
		else if (happiness > 99.9)
			this.happiness = 100;
		else
			this.happiness = happiness;
		
	}

	private double happiness;
	
	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Pelt pelt;
	
	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private PeltColor peltcolor;
	
	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Ear ear;
	
	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Body body;
	
	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private EyeShape eyeshape;
	
	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private PupilShape pupilshape;

	
	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private EyeColor eyecolor;
	
	public EyeColor getEyecolor() {
		return eyecolor;
	}

	public void setEyecolor(EyeColor eyecolor) {
		this.eyecolor = eyecolor;
	}

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Tail tail;
	
	public Cat getFather() {
		return father;
	}

	public void setFather(Cat father) {
		this.father = father;
	}

	public Cat getMother() {
		return mother;
	}

	public void setMother(Cat mother) {
		this.mother = mother;
	}

	private String basketid;
	
	private String catid;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Pelt getPelt() {
		return pelt;
	}

	public void setPelt(Pelt pelt) {
		this.pelt = pelt;
	}

	public PeltColor getPeltcolor() {
		return peltcolor;
	}

	public void setPeltcolor(PeltColor peltcolor) {
		this.peltcolor = peltcolor;
	}

	public Ear getEar() {
		return ear;
	}

	public void setEar(Ear ear) {
		this.ear = ear;
	}

	public Body getBody() {
		return body;
	}

	public void setBody(Body body) {
		this.body = body;
	}

	public EyeShape getEyeshape() {
		return eyeshape;
	}

	public void setEyeshape(EyeShape eyeshape) {
		this.eyeshape = eyeshape;
	}

	public PupilShape getPupilshape() {
		return pupilshape;
	}

	public void setPupilshape(PupilShape pupilshape) {
		this.pupilshape = pupilshape;
	}



	public Tail getTail() {
		return tail;
	}

	public void setTail(Tail tail) {
		this.tail = tail;
	}

	public String getBasketid() {
		return basketid;
	}

	public void setBasketid(String basketid) {
		this.basketid = basketid;
	}

	public String getCatid() {
		return catid;
	}

	public void setCatid(String catid) {
		this.catid = catid;
	}
	
	public int getAge()
	{
		if(birth != null)
		{
			long diff = System.currentTimeMillis() - birth.getTime();
			return (int)(diff / (24*3600 * 1000));
		}
		return 0;
	}
	
	public String getParentTraitString()
	{
		String returnstring = "";
		if(father != null)
			returnstring += "\nFather:"+father.getHoverString();
		if(mother != null)
			returnstring += "\nMother:"+mother.getHoverString();
		return returnstring.equals("")? "\nNo parents to show." : returnstring;
	}
	
	public String getHoverString()
	{
		String returnstring = "\n" + cattype.getName(); 
		returnstring += "\nPelt: " + pelt.getName();
		returnstring += "\nPelt Color: " + peltcolor.getName();
		returnstring += "\nBody: "+ body.getName();
		returnstring += "\nEars: " + ear.getName();
		returnstring += "\nTail: " + tail.getName();
		returnstring += "\nEye Shape: " + eyeshape.getName();
		returnstring += "\nPupil Shape: " + pupilshape.getName();
		returnstring += "\nEye Color: " + eyecolor.getName();
		
		return returnstring;
	}
	
	public String getTraitString()
	{
		String returnstring = "\n" + cattype.getName(); 
		returnstring += "\nGender: " + (male? "Male" : "Female");
		returnstring += "\nPelt: " + pelt.getName();
		returnstring += "\nPelt Color: " + peltcolor.getName();
		returnstring += "\nBody: "+ body.getName();
		returnstring += "\nEars: " + ear.getName();
		returnstring += "\nTail: " + tail.getName();
		returnstring += "\nEye Shape: " + eyeshape.getName();
		returnstring += "\nPupil Shape: " + pupilshape.getName();
		returnstring += "\nEye Color: " + eyecolor.getName();
		
		return returnstring;
	}
	
	public Element getProperties(Document doc)
	{

		Element rootElement = doc.createElement("Properties");
		
		Element idElement = doc.createElement("id");
		idElement.setTextContent(String.valueOf(id));
		rootElement.appendChild(idElement);
		
		Element genderElement = doc.createElement("gender");
		genderElement.setTextContent(male? "Male" : "Female");
		rootElement.appendChild(genderElement);
		
		Element cyclesElement = doc.createElement("cycles");
		cyclesElement.setTextContent(Integer.toString(cyclesleft));
		rootElement.appendChild(cyclesElement);
		
		Element nameElement = doc.createElement("name");
		nameElement.setTextContent(name);
		rootElement.appendChild(nameElement);
		List<Cat> babylist = this.getBabiesReadyForBirth();
		int babies = babylist == null ? 0 : babylist.size() ;
		if(babies > 0)
		{
			Element babiesReadyElement = doc.createElement("babiesready");
			babiesReadyElement.setTextContent(Long.toString(babylist.get(0).getId()));
			rootElement.appendChild(babiesReadyElement);
		}
		
		Element breedingElement = doc.createElement("breeding");
		breedingElement.setTextContent(this.getCatsettings().getBreedingString());
		rootElement.appendChild(breedingElement);
		
		Element birthElement = doc.createElement("birth");
		birthElement.setTextContent(catsettings.getBirthsettings() ? "On" : "Off");
		rootElement.appendChild(birthElement);
		
		Element hovernameElement = doc.createElement("hovername");
		hovernameElement.setTextContent(catsettings.getHovername() ? "On" : "Off");
		rootElement.appendChild(hovernameElement);
		
		Element hoverageElement = doc.createElement("hoverage");
		hoverageElement.setTextContent(catsettings.getHoverage() ? "On" : "Off");
		rootElement.appendChild(hoverageElement);
		
		Element hoverneedsElement = doc.createElement("hoverneeds");
		hoverneedsElement.setTextContent(catsettings.getHoverneeds() ? "On" : "Off");
		rootElement.appendChild(hoverneedsElement);
		
		Element hoverbreedingElement = doc.createElement("hoverbreeding");
		hoverbreedingElement.setTextContent(catsettings.getHoverbreeding() ? "On" : "Off");
		rootElement.appendChild(hoverbreedingElement);
				
		
		Element typeElement = doc.createElement("cattype");
		typeElement.setTextContent(cattype.getName());
		rootElement.appendChild(typeElement);
		
		Element ageElement = doc.createElement("age");
		ageElement.setTextContent(String.valueOf(getAge()));
		rootElement.appendChild(ageElement);
		
		Element hungerElement = doc.createElement("hunger");
		hungerElement.setTextContent(String.valueOf(Math.floor(hunger)));
		rootElement.appendChild(hungerElement);
		
		Element sickElement = doc.createElement("sick");
		sickElement.setTextContent(String.valueOf(sick));
		rootElement.appendChild(sickElement);
		
		Element healthPacksElement = doc.createElement("healthpacks");
		healthPacksElement.setTextContent(Integer.toString(owner.getHealthpack()));
		rootElement.appendChild(healthPacksElement);
		
		Element petPotionsElement = doc.createElement("petpotions");
		petPotionsElement.setTextContent(Integer.toString(owner.getPetpotions()));
		rootElement.appendChild(petPotionsElement);
		
		Element ispetElement = doc.createElement("ispet");
		ispetElement.setTextContent(String.valueOf(ispet));
		rootElement.appendChild(ispetElement);
		
		Element heatElement = doc.createElement("heat");
		heatElement.setTextContent(String.valueOf(Math.floor(heat)));
		rootElement.appendChild(heatElement);
		
		Element happinessElement = doc.createElement("happiness");
		happinessElement.setTextContent(String.valueOf(Math.floor(happiness)));
		rootElement.appendChild(happinessElement);
		
		if(pregnantfrom != null)
		{
			Element pregnancyElement = doc.createElement("pregnancy");
			pregnancyElement.setTextContent(String.valueOf(Math.floor(pregnancy)));
			rootElement.appendChild(pregnancyElement);
		}
		if(lastbaby != null)
		{
			long time = System.currentTimeMillis() - lastbaby.getTime();
			long hours = time % (3600 * 1000);
			if(24 - hours > 0)
			{
				Element pregnancyCooldownElement = doc.createElement("pregnancycooldown");
				pregnancyCooldownElement.setTextContent(String.valueOf(24-hours));
				rootElement.appendChild(pregnancyCooldownElement);
			}
		}
		
		return rootElement;
	}
	
	
	public Hashtable<String,Hashtable<String,String>> getParameters(EntityManager em)
	{
		try
		{
			int age = getAge();
			Hashtable<String,Hashtable<String,String>> parameters = cattype.getParameters(age);
			
			List<Hashtable<String,Hashtable<String,String>>> traitTables = new ArrayList<Hashtable<String,Hashtable<String,String>>>();
			if(body.parameteroverride)
			{
				traitTables.add(body.getParameters(age));
			}
			if(ear.parameteroverride)
			{
				traitTables.add(ear.getParameters(age));
			}
			if(tail.parameteroverride)
			{
				traitTables.add(tail.getParameters(age));
			}
			//merge the sculptmaps
			for(int i = 0;i<traitTables.size();i++)
			{
				Hashtable<String,Hashtable<String,String>> override = traitTables.get(i);
				Enumeration<String> keys = override.keys();
				String key;
				while(keys.hasMoreElements())
				{
					key = keys.nextElement();
					Hashtable<String,String> t = override.get(key);
					
					Enumeration<String> keys2 = t.keys();
					String key2;
					
					while(keys2.hasMoreElements())
					{
						key2=keys2.nextElement();
						
						override.get(key).put(key2,t.get(key2));
					}
				}
			}
			List<Hashtable<String,String>> textureTables = new ArrayList<Hashtable<String,String>>();
			textureTables.add(peltcolor.getParameters());
			textureTables.add(whiskers.getParameters());
			textureTables.add(((Eyes)em.createNamedQuery("getEyesByName").setParameter("name", Eyes.getQualifiedName(eyecolor, eyeshape, pupilshape)).getResultList().get(0)).getParameters());
			
			//and the textures
			for(int j = 0;j<textureTables.size();j++)
			{
				Hashtable<String,String> toverride = textureTables.get(j);
				Enumeration<String> keys = toverride.keys();
				String key;
				while(keys.hasMoreElements())
				{
					key = keys.nextElement();
					Hashtable<String,String> paramsTable = parameters.get(key);
					String param = toverride.get(key);
					paramsTable.put("texture", param);
				}
					
			}
			
			return parameters;
			
			
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
		
	}
	
	public Element getParametersElement(Document doc, EntityManager em)
	{
		Hashtable<String,Hashtable<String,String>> table = getParameters(em);
		
		Hashtable<String,String> newTable = new Hashtable();
		/* Parameters:
		 * types
		    sculptmap
			pos1
			pos2
			pos3
			size1
			size2
			size3
			rot
			mirror
			stitching
			texture
		 */
		//Let's build some strings!
		
		String types = "";
		String sculptmap = "";
		String pos = "" ;
		String size = "";
		String rot = "";
		String mirror = "";
		String stitching = "";
		String texture = "";
		String height1 = "";
		String height2 = "";
		String height3 = "";

		newTable.put("sculptmap", sculptmap);
		newTable.put("pos", pos);
		newTable.put("size", size);
		newTable.put("rot", rot);
		newTable.put("mirror", mirror);
		newTable.put("stitching", stitching);		
		newTable.put("texture", texture);
		newTable.put("height1", height1);
		newTable.put("height2", height2);
		newTable.put("height3", height3);

		
		
		Enumeration<String> typeskeys = table.keys();
		
		while(typeskeys.hasMoreElements())
		{
			String typeskey = typeskeys.nextElement();
			types += typeskey;
			
			if(typeskeys.hasMoreElements())
				types += "/";
			
			Hashtable<String,String> paramsTable = table.get(typeskey);			
			Enumeration<String> parameterkeys = paramsTable.keys();
			String parameterkey;
			while(parameterkeys.hasMoreElements())
			{
				parameterkey = parameterkeys.nextElement();
				String paramString = newTable.get(parameterkey);
				paramString += paramsTable.get(parameterkey);
				if(typeskeys.hasMoreElements())
					paramString += "/";
				newTable.put(parameterkey, paramString);
			}
		}
		newTable.put("types", types);
		
		
		
		
		Element rootElement = doc.createElement("Parameters");
		
		Enumeration<String> keys = newTable.keys();
		String key;
		while(keys.hasMoreElements())
		{
			key = keys.nextElement();
			Element element = doc.createElement(key);
			element.setTextContent(newTable.get(key));
			rootElement.appendChild(element);
			
		}
		
		return rootElement;
	}

	public void processHappiness(EntityManager em, Timestamp lu, Timestamp lhc, Timestamp now) {
		if(this.lastupdate.getTime() > lu.getTime() && lhc.getTime() > this.lasthappinesschange.getTime() && owner.getHappiness() >= 1 && hunger < 30  && !this.sick)
		{
			setHappiness(getHappiness() + Scheduler.GROWTH_RATE);
			setLasthappinesschange(now);
			owner.setHappiness(owner.getHappiness() - 1);
		}
		else if(lhc.getTime() > this.lasthappinesschange.getTime())
		{
			if(happiness > 0)
			{
				setHappiness(getHappiness() - Scheduler.GROWTH_RATE);
				setLasthappinesschange(now);
			}
		}
		em.persist(this);
	}
	
	public void processHunger(EntityManager em, Timestamp lu, Timestamp lhc, Timestamp now) {
		if(this.lastupdate.getTime() > lu.getTime() && lhc.getTime() > this.lasthungerchange.getTime() && owner.getFood() >= 1 && !this.sick)
		{
			setHunger(getHunger() - Scheduler.GROWTH_RATE);
			setLasthungerchange(now);
			owner.setFood(owner.getFood() - 1);
		}
		else if (lhc.getTime() > this.lasthungerchange.getTime() && !this.recentlyhealed())
		{
			if(hunger < 100)
			{
				setHunger(getHunger() + Scheduler.GROWTH_RATE);
				setLasthungerchange(now);
			}
		}
		em.persist(this);
	}

	public void processHeat(EntityManager em, Timestamp lu, Timestamp lhc, Timestamp now) {
		if(getAge() >=5 && this.lastupdate.getTime() > lu.getTime() && lhc.getTime() > this.lastheatchange.getTime())
		{
			if(male || (pregnantfrom == null && (lastbaby == null || now.getTime() - lastbaby.getTime() >= (24 * 3600000))) && cyclesleft > 0  && !this.sick)
			{
				setHeat(getHeat() + Scheduler.GROWTH_RATE);
				setLastheatchange(now);
				em.persist(this);
			}
		}
		
	}

	public void processPregnancy(EntityManager em, Timestamp lu, Timestamp lhc, Timestamp now) {
			Cat baby = pregnantwith;
			double newPregnancy = getPregnancy() + Scheduler.GROWTH_RATE;
			setPregnancy(newPregnancy);
			setLastpregnancychange(now);
			if(getPregnancy() == 0.0)
			{
				setLastbaby(now);
				baby.setReadyforbirth(true);
				em.persist(baby);
			}
			em.persist(this);
	}

	public Whiskers getWhiskers() {
		return whiskers;
	}

	public void setWhiskers(Whiskers whiskers) {
		this.whiskers = whiskers;
	}

	public double getHeight1() {
		return cattype.getHeight1();
	}
	
	public boolean canBreed(Cat partner)
	{
		if(this.male != partner.male)
		{
			if(this.getCatsettings().getBreedingsettings() == BreedingSettings.OwnerOnly)
			{
				if(this.getOwner().getId().equals(partner.getOwner().getId()))
				{
					return true;
				}
			}
			if(this.getCatsettings().getBreedingsettings() == BreedingSettings.GroupOnly)
			{
				if(this.groupid.equals(partner.groupid))
				{
					return true;
				}
			}
			if(this.getCatsettings().getBreedingsettings() == BreedingSettings.Everyone)
			{
				return true;
			}
			if(this.getCatsettings().getBreedingsettings() == BreedingSettings.Mate)
			{
				if(this.getCatsettings().getMate() != null && this.getCatsettings().getMate().getCatid().equals(partner.getCatid()))
				{
					return true;
				}
			}
		}
		
		return false;
	}
	
	public static boolean canBreed(Cat one, Cat two)
	{
		return one.canBreed(two) && two.canBreed(one);
	}

	public void birthCheck() {
		if(this.birth == null)
		{
			birth = new Timestamp(System.currentTimeMillis());
			lasthungerchange = new Timestamp(System.currentTimeMillis());
			lastheatchange = new Timestamp(System.currentTimeMillis());
			lasthappinesschange = new Timestamp(System.currentTimeMillis());
		}
		
	}

	public Element getParametersElement(Document doc, EntityManager em, int growth) {
Hashtable<String,Hashtable<String,String>> table = getParameters(em,growth);
		
		Hashtable<String,String> newTable = new Hashtable();
		/* Parameters:
		 * types
		    sculptmap
			pos1
			pos2
			pos3
			size1
			size2
			size3
			rot
			mirror
			stitching
			texture
		 */
		//Let's build some strings!
		
		String types = "";
		String sculptmap = "";
		String pos = "" ;
		String size = "";
		String rot = "";
		String mirror = "";
		String stitching = "";
		String texture = "";
		String height1 = "";
		String height2 = "";
		String height3 = "";

		newTable.put("sculptmap", sculptmap);
		newTable.put("pos", pos);
		newTable.put("size", size);
		newTable.put("rot", rot);
		newTable.put("mirror", mirror);
		newTable.put("stitching", stitching);		
		newTable.put("texture", texture);
		newTable.put("height1", height1);
		newTable.put("height2", height2);
		newTable.put("height3", height3);

		
		
		Enumeration<String> typeskeys = table.keys();
		
		while(typeskeys.hasMoreElements())
		{
			String typeskey = typeskeys.nextElement();
			types += typeskey;
			
			if(typeskeys.hasMoreElements())
				types += "/";
			
			Hashtable<String,String> paramsTable = table.get(typeskey);			
			Enumeration<String> parameterkeys = paramsTable.keys();
			String parameterkey;
			while(parameterkeys.hasMoreElements())
			{
				parameterkey = parameterkeys.nextElement();
				String paramString = newTable.get(parameterkey);
				paramString += paramsTable.get(parameterkey);
				if(typeskeys.hasMoreElements())
					paramString += "/";
				newTable.put(parameterkey, paramString);
			}
		}
		newTable.put("types", types);
		
		
		
		
		Element rootElement = doc.createElement("Parameters");
		
		Enumeration<String> keys = newTable.keys();
		String key;
		while(keys.hasMoreElements())
		{
			key = keys.nextElement();
			Element element = doc.createElement(key);
			element.setTextContent(newTable.get(key));
			rootElement.appendChild(element);
			
		}
		
		return rootElement;
	}

	private Hashtable<String, Hashtable<String, String>> getParameters(EntityManager em, int growth) {
		try
		{
			int age = 0;
			if(growth == 1)
			{
				age = 0;
			}
			else if (growth == 2)
			{
				age = 3;
			}
			else if (growth == 3)
			{
				age = 5;
			}
			Hashtable<String,Hashtable<String,String>> parameters = cattype.getParameters(age);
			
			List<Hashtable<String,Hashtable<String,String>>> traitTables = new ArrayList<Hashtable<String,Hashtable<String,String>>>();
			if(body.parameteroverride)
			{
				traitTables.add(body.getParameters(age));
			}
			if(ear.parameteroverride)
			{
				traitTables.add(ear.getParameters(age));
			}
			if(tail.parameteroverride)
			{
				traitTables.add(tail.getParameters(age));
			}
			//merge the sculptmaps
			for(int i = 0;i<traitTables.size();i++)
			{
				Hashtable<String,Hashtable<String,String>> override = traitTables.get(i);
				Enumeration<String> keys = override.keys();
				String key;
				while(keys.hasMoreElements())
				{
					key = keys.nextElement();
					Hashtable<String,String> t = override.get(key);
					
					Enumeration<String> keys2 = t.keys();
					String key2;
					
					while(keys2.hasMoreElements())
					{
						key2=keys2.nextElement();
						
						override.get(key).put(key2,t.get(key2));
					}
				}
			}
			List<Hashtable<String,String>> textureTables = new ArrayList<Hashtable<String,String>>();
			textureTables.add(peltcolor.getParameters());
			textureTables.add(whiskers.getParameters());
			textureTables.add(((Eyes)em.createNamedQuery("getEyesByName").setParameter("name", Eyes.getQualifiedName(eyecolor, eyeshape, pupilshape)).getResultList().get(0)).getParameters());
			
			//and the textures
			for(int j = 0;j<textureTables.size();j++)
			{
				Hashtable<String,String> toverride = textureTables.get(j);
				Enumeration<String> keys = toverride.keys();
				String key;
				while(keys.hasMoreElements())
				{
					key = keys.nextElement();
					Hashtable<String,String> paramsTable = parameters.get(key);
					String param = toverride.get(key);
					paramsTable.put("texture", param);
				}
					
			}
			
			return parameters;
			
			
		}catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return null;
	}
}
