package com.feistyfelinesbreedables.portal.entity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@NamedQueries({
@NamedQuery(name="getAllBodies", query = "SELECT b FROM Body b"),
@NamedQuery(name="getBodyByName",query="SELECT b FROM Body b WHERE b.name = :name"),
@NamedQuery(name="getBodyStarter",query="SELECT b FROM Body b WHERE b.traittype = com.feistyfelinesbreedables.portal.entity.TraitType.Starter"),
@NamedQuery(name="getBodyBreeding",query="SELECT b FROM Body b WHERE b.traittype = com.feistyfelinesbreedables.portal.entity.TraitType.Breeding"),
@NamedQuery(name="getBodyById", query="SELECT b FROM Body b WHERE b.id= :id")
})
@Table(name="body")
public class Body extends SculptTrait{
	public Body(String name, String sculpttypes, String sculptmaps)
	{
		super(name, sculpttypes,sculptmaps);
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public CatType getCattype() {
		return cattype;
	}

	public void setCattype(CatType cattype) {
		this.cattype = cattype;
	}

	protected Body()
	{
		
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;

	
	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private CatType cattype;
}
