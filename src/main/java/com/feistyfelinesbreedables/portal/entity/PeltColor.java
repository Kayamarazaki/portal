package com.feistyfelinesbreedables.portal.entity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@NamedQueries({
@NamedQuery(name="getPeltColorByName",query="SELECT pc FROM PeltColor pc WHERE pc.name = :name"),
@NamedQuery(name="getPeltColorStarter",query="SELECT p FROM PeltColor p WHERE p.traittype = com.feistyfelinesbreedables.portal.entity.TraitType.Starter AND p.pelt = :pelt"),
@NamedQuery(name="getPeltColorBreeding",query="SELECT p FROM PeltColor p WHERE p.traittype = com.feistyfelinesbreedables.portal.entity.TraitType.Breeding"),
@NamedQuery(name="getPeltColorById", query="SELECT pc FROM PeltColor pc WHERE pc.id= :id")
})
@Table(name = "peltcolor")
public class PeltColor extends TextureTrait{
	public PeltColor(String name, Pelt pelt, String texturemaps, String texturetypes)
	{
		super(name,texturemaps,texturetypes);
		this.pelt = pelt;
	}
	
	protected PeltColor()
	{
	
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	
	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Pelt pelt;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Pelt getPelt() {
		return pelt;
	}

	public void setPelt(Pelt pelt) {
		this.pelt = pelt;
	}
}
