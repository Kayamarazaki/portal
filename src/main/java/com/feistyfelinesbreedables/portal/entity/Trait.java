package com.feistyfelinesbreedables.portal.entity;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public class Trait {
	protected Trait()
	{
		
	}
	public Trait(String name)
	{
		this.name = name;
	}
	
	protected int occurence;
	
	protected boolean hidden;
	
	protected String name;

	protected String description;
	
	
	public boolean isHidden() {
		return hidden;
	}
	public void setHidden(boolean hidden) {
		this.hidden = hidden;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}

	@Enumerated(EnumType.ORDINAL)
	protected TraitType traittype;
	
	public TraitType getTraittype() {
		return traittype;
	}
	public void setTraittype(TraitType traittype) {
		this.traittype = traittype;
	}
	public String getName() {
		return name;
	}

	public int getOccurence() {
		return occurence;
	}
	public void setOccurence(int occurence) {
		this.occurence = occurence;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	
}
