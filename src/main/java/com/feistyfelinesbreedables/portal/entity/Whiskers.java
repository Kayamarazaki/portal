package com.feistyfelinesbreedables.portal.entity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@NamedQueries({
	@NamedQuery(name="getAllWhiskers", query = "SELECT w FROM Whiskers w"),
@NamedQuery(name="getWhiskersByName",query="SELECT w FROM Whiskers w WHERE w.name = :name"),
@NamedQuery(name="getWhiskersStarter",query="SELECT w FROM Whiskers w WHERE w.traittype = com.feistyfelinesbreedables.portal.entity.TraitType.Starter"),
@NamedQuery(name="getWhiskersBreeding",query="SELECT w FROM Whiskers w WHERE w.traittype = com.feistyfelinesbreedables.portal.entity.TraitType.Breeding"),
@NamedQuery(name="getWhiskersById", query="SELECT w FROM Whiskers w WHERE w.id= :id")
})
@Table(name="whiskers")
public class Whiskers extends TextureTrait{
	public Whiskers(String name, String texturetypes, String texturemaps)
	{
		super(name, texturetypes,texturemaps);
	}
	
	protected Whiskers()
	{
		
	}
	
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public CatType getCattype() {
		return cattype;
	}

	public void setCattype(CatType cattype) {
		this.cattype = cattype;
	}


	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;

	
	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private CatType cattype;
}

