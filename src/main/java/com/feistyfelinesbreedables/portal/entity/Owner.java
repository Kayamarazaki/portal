package com.feistyfelinesbreedables.portal.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "owner")
@NamedQueries({ @NamedQuery(name = "getOwnerById", query = "SELECT o FROM Owner o WHERE o.id = :id") })
public class Owner {
	public Owner(String id) {
		this();
		this.id = id;
	}

	protected Owner() {

	}

	@Id
	private String id;

	private int food;

	private int happiness;
	
	private int pawpoints;

	private int petpotions;
	
	public int getPetpotions() {
		return petpotions;
	}

	public void setPetpotions(int petpotions) {
		this.petpotions = petpotions;
	}
	
	private int HibernateBasket;
	
	public int getHibernateBasket() {
		return HibernateBasket;
	}
	
	public void setHibernateBasket(int HibernateBasket) {
		this.HibernateBasket = HibernateBasket;
	}

	private int healthpack;
	@OneToMany(mappedBy = "owner",fetch=FetchType.LAZY)
	private List<Cat> cats;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public int getFood() {
		return food;
	}

	public void setFood(int food) {
		this.food = food;
	}

	public int getHappiness() {
		return happiness;
	}

	public void setHappiness(int happiness) {
		this.happiness = happiness;
	}

	public int getHealthpack() {
		return healthpack;
	}

	public void setHealthpack(int healthpack) {
		this.healthpack = healthpack;
	}

	public List<Cat> getCats() {
		return cats;
	}

	public void setCats(List<Cat> cats) {
		this.cats = cats;
	}

	public int getPawpoints() {
		return pawpoints;
	}

	public void setPawpoints(int pawpoints) {
		this.pawpoints = pawpoints;
	}
	public static Owner getOrCreateOwner(String id, EntityManager em) {
		List li = em.createNamedQuery("getOwnerById").setParameter("id", id).getResultList();
		if (li.isEmpty()) {
			Owner o = new Owner(id);
			em.persist(o);
			return o;
		} else {
			return (Owner) li.get(0);
		}
	}

	public boolean equals(Object o) {
		if (o instanceof Owner) {
			if (((Owner) o).id.equals(this.id))
				return true;
		}
		return false;
	}


	
	
}
