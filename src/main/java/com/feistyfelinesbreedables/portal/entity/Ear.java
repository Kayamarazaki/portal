package com.feistyfelinesbreedables.portal.entity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@NamedQueries({
@NamedQuery(name="getAllEars", query = "SELECT e FROM Ear e"),
@NamedQuery(name="getEarByName",query="SELECT e FROM Ear e WHERE e.name = :name"),
@NamedQuery(name="getEarStarter",query="SELECT e FROM Ear e WHERE e.traittype = com.feistyfelinesbreedables.portal.entity.TraitType.Starter"),
@NamedQuery(name="getEarBreeding",query="SELECT e FROM Ear e WHERE e.traittype = com.feistyfelinesbreedables.portal.entity.TraitType.Breeding"),
@NamedQuery(name="getEarById", query="SELECT e FROM Ear e WHERE e.id= :id")
})
@Table(name="ear")
public class Ear extends SculptTrait{
	public Ear(String name, String sculpttypes, String sculptmaps)
	{
		super(name, sculpttypes,sculptmaps);
	}
	
	protected Ear()
	{
		
	}
	
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public CatType getCattype() {
		return cattype;
	}

	public void setCattype(CatType cattype) {
		this.cattype = cattype;
	}


	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;

	
	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private CatType cattype;
}
