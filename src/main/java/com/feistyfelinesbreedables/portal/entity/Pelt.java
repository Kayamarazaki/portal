package com.feistyfelinesbreedables.portal.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@NamedQueries({
@NamedQuery(name="getAllPelts",query="SELECT p FROM Pelt p"),
@NamedQuery(name="getPeltByName",query="SELECT p FROM Pelt p WHERE p.name = :name"),
@NamedQuery(name="getPeltStarter",query="SELECT p FROM Pelt p WHERE p.traittype = com.feistyfelinesbreedables.portal.entity.TraitType.Starter"),
@NamedQuery(name="getPeltBreeding",query="SELECT p FROM Pelt p WHERE p.traittype = com.feistyfelinesbreedables.portal.entity.TraitType.Breeding")
})
@Table(name = "pelt")
public class Pelt extends Trait {
	public Pelt(String name)
	{
		super(name);
	}
	
	protected Pelt()
	{
		
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public List<PeltColor> getPeltcolors() {
		return peltcolors;
	}

	public void setPeltcolors(List<PeltColor> peltcolors) {
		this.peltcolors = peltcolors;
	}

	public CatType getCattype() {
		return cattype;
	}

	public void setCattype(CatType cattype) {
		this.cattype = cattype;
	}

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	
	@OneToMany(mappedBy = "pelt")
	private List<PeltColor> peltcolors;
	
	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private CatType cattype;
}
