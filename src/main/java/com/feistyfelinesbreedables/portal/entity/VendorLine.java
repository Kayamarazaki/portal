package com.feistyfelinesbreedables.portal.entity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "vendorline")
public class VendorLine {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	
	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name= "vendorobject_id")
	private VendorObject vendorobject;
	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name= "vendor_id")
	private Vendor vendor;
	
	private int amount;
	
	public VendorLine(Vendor vendor, VendorObject vendorobject, int amount)
	{
		this.vendor=vendor;
		this.vendorobject=vendorobject;
		this.amount=amount;
	}
	
	protected VendorLine()
	{
		
	}
	
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public VendorObject getVendorobject() {
		return vendorobject;
	}

	public void setVendorobject(VendorObject vendorobject) {
		this.vendorobject = vendorobject;
	}

	public Vendor getVendor() {
		return vendor;
	}

	public void setVendor(Vendor vendor) {
		this.vendor = vendor;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public String toString()
	{
		String result = "<VendorLine><id>" + id + "</id>";
		result += vendorobject.toString();
		result += String.format("<amount>%s</amount>", String.valueOf(amount));
		result += "</VendorLine>";
		return result;
	}
}
