package com.feistyfelinesbreedables.portal.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@NamedQueries({
	@NamedQuery(name="getAllPupilShapes", query = "SELECT p FROM PupilShape p"),
@NamedQuery(name="getPupilShapeByName",query="SELECT ps FROM PupilShape ps WHERE ps.name = :name"),
@NamedQuery(name="getPupilShapeStarter",query="SELECT p FROM PupilShape p WHERE p.traittype = com.feistyfelinesbreedables.portal.entity.TraitType.Starter"),
@NamedQuery(name="getPupilShapeBreeding",query="SELECT p FROM PupilShape p WHERE p.traittype = com.feistyfelinesbreedables.portal.entity.TraitType.Breeding"),
@NamedQuery(name="getPupilShapeById", query="SELECT ps FROM PupilShape ps WHERE ps.id= :id")
})
@Table(name="pupilshape")
public class PupilShape extends Trait {
	public PupilShape(String name)
	{
		super(name);
	}
	
	protected PupilShape()
	{
	
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
}
