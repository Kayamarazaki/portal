package com.feistyfelinesbreedables.portal.entity;

import java.util.Hashtable;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
@MappedSuperclass
public class SculptTrait extends Trait {
	protected SculptTrait()
	{
		
	}
	
	public SculptTrait(String name, String sculpttypes, String sculptmaps)
	{
		super(name);
		this.sculpttypes = sculpttypes;
		this.sculptmaps = sculptmaps;
	}
	
	protected boolean parameteroverride;
	@Column(length=1000)
	protected String position_gen_1;
	@Column(length=1000)
	protected String position_gen_2;
	@Column(length=1000)
	protected String position_gen_3;
	@Column(length=1000)
	protected String size_gen_1;
	@Column(length=1000)
	protected String size_gen_2;
	@Column(length=1000)
	protected String size_gen_3;
	@Column(length=1000)
	protected String rotation;
	@Column(length=1000)
	protected String mirror;
	@Column(length=1000)
	protected String stitching;
	@Column(length=1000)
	protected String sculpttypes;
	@Column(length=1000)
	protected String sculptmaps;
	public boolean isParameteroverride() {
		return parameteroverride;
	}

	public void setParameteroverride(boolean parametersOverride) {
		this.parameteroverride = parametersOverride;
	}

	public String getPosition_gen_1() {
		return position_gen_1;
	}

	public void setPosition_gen_1(String position_gen_1) {
		this.position_gen_1 = position_gen_1;
	}

	public String getPosition_gen_2() {
		return position_gen_2;
	}

	public void setPosition_gen_2(String position_gen_2) {
		this.position_gen_2 = position_gen_2;
	}

	public String getPosition_gen_3() {
		return position_gen_3;
	}

	public void setPosition_gen_3(String position_gen_3) {
		this.position_gen_3 = position_gen_3;
	}

	public String getSize_gen_1() {
		return size_gen_1;
	}

	public void setSize_gen_1(String size_gen_1) {
		this.size_gen_1 = size_gen_1;
	}

	public String getSize_gen_2() {
		return size_gen_2;
	}

	public void setSize_gen_2(String size_gen_2) {
		this.size_gen_2 = size_gen_2;
	}

	public String getSize_gen_3() {
		return size_gen_3;
	}

	public void setSize_gen_3(String size_gen_3) {
		this.size_gen_3 = size_gen_3;
	}

	public String getRotation() {
		return rotation;
	}

	public void setRotation(String rotation) {
		this.rotation = rotation;
	}

	public String getMirror() {
		return mirror;
	}

	public void setMirror(String mirror) {
		this.mirror = mirror;
	}

	public String getStitching() {
		return stitching;
	}

	public void setStitching(String stitching) {
		this.stitching = stitching;
	}

	public String getSculpttypes() {
		return sculpttypes;
	}

	public void setSculpttypes(String sculpttypes) {
		this.sculpttypes = sculpttypes;
	}

	public String getSculptmaps() {
		return sculptmaps;
	}

	public void setSculptmaps(String sculptmaps) {
		this.sculptmaps = sculptmaps;
	}
	
	public Element getTraitInfo(Document doc)
	{
		Element traitElement = doc.createElement("Trait");
		
		Element nameElement = doc.createElement("name");
		traitElement.appendChild(nameElement);
		nameElement.setTextContent(name);
		
		return traitElement;
	}
	
	public Hashtable<String,Hashtable<String,String>> getParameters(int age) throws Exception
	{
		if(parameteroverride)
		{
			Hashtable<String, Hashtable<String,String>> table = new Hashtable();
			
			String[] types = sculpttypes.replace("[", "").replace("]", "").split("/");
			String[] maps = sculptmaps.replace("[", "").replace("]", "").split("/");
			String[] pos1 = position_gen_1.replace("[", "").replace("]", "").split("/");
			String[] pos2 = position_gen_2.replace("[", "").replace("]", "").split("/");
			String[] pos3 = position_gen_3.replace("[", "").replace("]", "").split("/");
			String[] size1 = size_gen_1.replace("[", "").replace("]", "").split("/");
			String[] size2 = size_gen_2.replace("[", "").replace("]", "").split("/");
			String[] size3 = size_gen_3.replace("[", "").replace("]", "").split("/");
			String[] rots = rotation.replace("[", "").replace("]", "").split("/");
			String[] mirr = mirror.split("/");
			String[] stitch = stitching.split("/");

			if(types.length == maps.length && types.length == pos1.length&& types.length == pos2.length && types.length == pos3.length && types.length == size1.length && types.length == size2.length && types.length == size3.length && types.length == rots.length && types.length == mirr.length && types.length == stitch.length)
			{
				for(int i = 0;i<types.length;i++)
				{
					Hashtable<String,String> t = new Hashtable();
					
					t.put("sculptmap", maps[i]);
					if(age < 3)
						t.put("pos", pos1[i]);
					else if (age < 5)
						t.put("pos", pos2[i]);
					else
						t.put("pos", pos3[i]);
					if(age < 3)
						t.put("size", size1[i]);
					else if (age < 5)
						t.put("size", size2[i]);
					else
						t.put("size", size3[i]);
					t.put("rot", rots[i]);
					t.put("mirror", mirr[i]);
					t.put("stitching", stitch[i]);
					
					table.put(types[i], t);
				}
				
			}
			else
			{
				throw new Exception("Wrong length of arrays");
			}
			
			return table;
			}
		else
		{
			return null;
		}
	}

}
