package com.feistyfelinesbreedables.portal.entity;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "vendor")
public class Vendor {
	public Vendor(String name, int price)
	{
		this.name = name;
		this.price = price;
	}
	
	protected Vendor()
	{
		
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public void setVendorlines(List<VendorLine> vendorlines) {
		this.vendorlines = vendorlines;
	}

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	private Date expirationdate;
	
	private String name;
	
	private int price;
	
	@OneToMany( mappedBy= "vendor")
	private List<VendorLine> vendorlines;
	
	@Override
	public String toString()
	{
		String result = "<Vendor><id>" + id + "</id><price>"+ price + "</price><VendorLines>";
		for(VendorLine line : vendorlines)
		{
			result += line.toString();
		}
		result += "</VendorLines></Vendor>";
		return result;
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Date getExpirationdate() {
		return expirationdate;
	}

	public void setExpirationdate(Date expirationdate) {
		this.expirationdate = expirationdate;
	}

	public List<VendorLine> getVendorlines() {
		return vendorlines;
	}
}
