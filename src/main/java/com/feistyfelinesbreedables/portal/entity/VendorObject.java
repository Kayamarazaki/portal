package com.feistyfelinesbreedables.portal.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@NamedQuery(name = "getVendorObjectByName", query ="SELECT vo FROM VendorObject vo WHERE vo.name = :name")
@Table(name = "vendorobject")
public class VendorObject {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	
	private String name;
	
	public VendorObject(String name, String objectname)
	{
		this.name = name;
		this.objectname = objectname;
	}
	
	protected VendorObject()
	{
		
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		name = name;
	}

	public String getObjectName() {
		return objectname;
	}

	public void setObjectName(String objectName) {
		objectname = objectName;
	}

	private String objectname;
	
	public String toString()
	{
		return "<VendorObject><Name>" + name + "</Name><ObjectName>" + objectname + "</ObjectName></VendorObject>";
	}
}
