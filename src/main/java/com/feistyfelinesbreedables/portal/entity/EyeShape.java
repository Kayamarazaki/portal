package com.feistyfelinesbreedables.portal.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@NamedQueries({
	@NamedQuery(name="getAllEyeShapes", query = "SELECT e FROM EyeShape e"),
@NamedQuery(name="getEyeShapeByName",query="SELECT e FROM EyeShape e WHERE e.name = :name"),
@NamedQuery(name="getEyeShapeStarter",query="SELECT e FROM EyeShape e WHERE e.traittype = com.feistyfelinesbreedables.portal.entity.TraitType.Starter"),
@NamedQuery(name="getEyeShapeBreeding",query="SELECT e FROM EyeShape e WHERE e.traittype = com.feistyfelinesbreedables.portal.entity.TraitType.Breeding"),
@NamedQuery(name="getEyeShapeById", query="SELECT es FROM EyeShape es WHERE es.id= :id")
})
@Table(name="eyeshape")
public class EyeShape extends Trait {
	public EyeShape(String name)
	{
		super(name);
	}
	
	protected EyeShape()
	{
	
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
}
