package com.feistyfelinesbreedables.portal;

public enum BreedingSettings {
    Everyone(0),
    GroupOnly(1),
    OwnerOnly(2),
    Mate(3);
	
	private final int value;
    private BreedingSettings(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
    
}
